﻿namespace DaLiSaConnectApi.Helpers
{
    public static class AppConstant
    {
        public const string Success = "succeeded";
        public const string Pending = "pending";
        public const string Fail = "failed";
        public const string CurrencyType = "usd";
        public const string Oneimage = "oneimage";
        public const string Tenimage = "tenimage";
        public const string Oneweek = "oneweek";
        public const string Onemonth = "onemonth";
        public const string Topweek = "topweek";
        public const string Topmonth = "topmonth";
        public const string FilePath = "Log.txt";
        public const string FolderPath = "LogFile";
    }
}
