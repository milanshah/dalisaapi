﻿namespace DaLiSaConnectApi.Helpers
{
    public class AppEnums
    {
        public enum ServiceStatus
        {
            NewOrOngoingService = 1,
            RenewService = 2,
            TopListService = 3,
            ExpireService = 4
        }

    }
}
