﻿using System;
using System.Data;
using System.Web;
using System.IO;
using Microsoft.AspNetCore.Hosting;
namespace DaLiSaConnectApi.Helpers
{
    public class AppLog
    {
        /// <summary>
        /// Write a application log file
        /// </summary>
        /// <param name="messageToLog"></param>
        public static void ApplicationLog(string messageToLog,string path)
        {
            //StreamWriter streamWriter = null;
            //var path = Directory.GetCurrentDirectory();
            //var FilePath = path + AppConstant.FolderPath + AppConstant.FilePath;

            //if (!File.Exists(strPath))
            //    streamWriter = new StreamWriter(strPath);
            //else
            //    streamWriter = File.AppendText(strPath);

            //streamWriter.WriteLine(string.Format("[{0}] {1}", DateTime.Now.ToString(), messageToLog) + Environment.NewLine);


            var FilePath = Path.Combine(path, AppConstant.FolderPath);
            string strPath = FilePath;

            using (StreamWriter outputFile = new StreamWriter(Path.Combine(strPath, AppConstant.FilePath), true))
            {
                outputFile.WriteLine(string.Format("[{0}] {1}", DateTime.Now.ToString(), messageToLog) + Environment.NewLine);
            }
        }
    }
}
