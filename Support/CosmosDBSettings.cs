public class CosmosDBSettings
{
    public const string SectionName = "CosmosDBSettings";

    public string ConnectionUrl { get; set; } = "";
    public string DatabaseName { get; set; } = "";
}