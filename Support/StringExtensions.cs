using System.Linq;

public static class StringExtensions 
{
    public static bool IsHex(this char ch)
    {
        var test = char.ToUpper(ch);
        return char.IsDigit(ch) || (test >= 'A' && test <= 'F');
    }

    public static bool IsHex(this string value)
    {
        return value.All(ch => ch.IsHex());
    }
}