using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.JsonPatch.Operations;
using Microsoft.AspNetCore.Mvc.ModelBinding;

public static class JsonPatchExtensions
{
    public static void PreventPatchingProperty<TModel>(this JsonPatchDocument<TModel> document, string property, ModelStateDictionary model) where TModel : class
    {
        document.PreventPatchingProperties(new List<string>{ property }, model);
    }
    
    public static void PreventPatchingProperties<TModel>(this JsonPatchDocument<TModel> document, IEnumerable<string> properties, ModelStateDictionary model) where TModel : class
    {
        var modifyingOps = document.Operations.SkipWhile(op => op.OperationType == OperationType.Test);
        
        foreach (var property in properties)
        {
            if (modifyingOps.Any(operation => operation.path.Trim('/').Equals(property, StringComparison.OrdinalIgnoreCase)))
            {
                model.AddModelError(property, $"property '{property}' is not modifiable");
            }
        }
    }
}