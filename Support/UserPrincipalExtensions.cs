using System;
using System.Security.Claims;

public static class UserExtensions {
    private const string ObjectIdClaimType = "http://schemas.microsoft.com/identity/claims/objectidentifier";

    public static string? TryGetObjectId(this ClaimsPrincipal principal) {
        var id = principal.FindFirst(claim => claim.Type == ObjectIdClaimType)?.Value;
        
        if (id != null)
        {
            var guid = Guid.Parse(id);
            id = guid.ToString("N");
        }
        
        return id;
    }
}