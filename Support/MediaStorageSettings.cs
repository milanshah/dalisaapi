public class MediaStorageSettings
{
    public const string SectionName = "MediaStorageSettings";

    public string AccountName { get; set; } = "";

    public string AccountKey { get; set; } = "";
    
    public string ContainerName { get; set; } = "";
}