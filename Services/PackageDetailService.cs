using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DaLiSaConnectApi.Helpers;

public class PackageDetailService
{
    private readonly IPackageDetailRepository _repository;

    public PackageDetailService(IPackageDetailRepository repository) { _repository = repository; }

    internal async Task<IEnumerable<Packages>> GetPackages() => await _repository.getPackages();

}