using System.Collections.Generic;
using System.Threading.Tasks;

public class IndustryService {
    private readonly IIndustryRepository _repository;

    public IndustryService(IIndustryRepository repository) { _repository = repository; }

    internal async Task<IEnumerable<IndustryCategory>> GetAllIndustryCategoriesAsync()
    {
        return await _repository.GetIndustryCategoriesAsync();
    }
}