using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Geolocation;

public class TrendingRecentService
{
    private readonly ITrendingRecentIndustryRepository _repository;

    public TrendingRecentService(ITrendingRecentIndustryRepository repository) { _repository = repository;}

    #region GetAllTrendingNearby
    internal async Task<IEnumerable<TrendingRecentIndustry>> GetAllTrendingNearby(double latitude, double longitude)
    {
        var LstTrending = await _repository.GetAllTrending();
        var results = new List<TrendingRecentIndustry>();

        results = (from list in LstTrending
                   where list.Latitude == latitude && list.Longitude == longitude
                   orderby list.Seachcount descending
                   select list).Distinct().Take(10).ToList();

        return results;
    }
    #endregion

    #region GetAllRecentSearch
    internal async Task<IEnumerable<TrendingRecentIndustry>> GetAllRecentSearch(string userID)
    {
        var LstTrending = await _repository.GetAllTrending();
        var results = new List<TrendingRecentIndustry>();

        results = (from list in LstTrending
                   where list.UserId == userID && list.Datetime == DateTime.Today.ToString("dd/MM/yyyy")
                   orderby list.Id descending
                   select list).Distinct().Take(10).ToList();

        return results;
    }
    #endregion
}