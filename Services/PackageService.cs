using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DaLiSaConnectApi.Helpers;

public class PackageService
{
    private readonly IPackageRepository _repository;

    private readonly ISkillProfileRepository _skillrepository;

    public PackageService(IPackageRepository repository, ISkillProfileRepository skillrepository) 
    {
        _repository = repository;
        _skillrepository = skillrepository;
    }

    #region StorePackageInfo
    internal async Task<PackageInfo> StorePackageInfo(PackageDetails request) => await _repository.InsertPackageInfo(request);
    #endregion

    #region TryGetPackageIdAsync
    internal async Task<PackageInfo?> TryGetPackageIdAsync(string id) => await _repository.TryGetPackageIdAsync(id);
    #endregion

    #region GetPackageList
    internal async Task<IEnumerable<PackageInfo>> GetPackageList() => await _repository.GetPackageList();
    #endregion

    // internal async Task<PackageInfo> SaveUpdatedPackageInfo(PackageInfo updated) => await _repository.UpdatePackageInfoAsync(updated);

    #region TryExpireByUserId
    internal async Task<bool> TryExpireByUserId(string userId) {

        if (userId != null)
        {
            var lst = await _skillrepository.TryGetListSkillByUserIdAsync(userId ?? "");
            #region LoopForService
            for (int index = 0; index < lst.Count; index++)
            {
                var serviceId = lst[index].Id;

                var dateTime = DateTime.UtcNow;
                var UserPackages = await GetPackageList();
                var ListPackage = (from p in UserPackages
                                   where p.ServiceID == serviceId
                                   select p).ToList();

                var objskill = new SkillProfile();
                var ActivePackage = ListPackage.Where(p => p.PackageStatus != Convert.ToInt32(AppEnums.ServiceStatus.ExpireService)).ToList();
                if (ActivePackage.Count > 0)
                {
                    #region LoopForActivePackage
                    for (int j = 0; j < ActivePackage.Count; j++)
                    {
                        var removeflag = false;
                        var resval = DateTime.Compare(dateTime, Convert.ToDateTime(ActivePackage[j].ExpireTime));
                        objskill = await _skillrepository.TryGetSkillProfileByIdAsync(serviceId);
                        if (objskill != null)
                        {
                            if (resval > 0)
                            {
                                var imguploaded = lst[index].WorkImages.Where(x => x.PackageID == ActivePackage[j].Id).Count();
                                if (ActivePackage[j].PackageName == AppConstant.Oneimage)
                                {
                                    ActivePackage[j].PackageStatus = Convert.ToInt32(AppEnums.ServiceStatus.ExpireService);
                                    if (objskill.PackageStatus != Convert.ToInt32(AppEnums.ServiceStatus.ExpireService))
                                    {
                                        removeflag = true;
                                    }
                                }
                                else if (ActivePackage[j].PackageName == AppConstant.Tenimage)
                                {
                                    ActivePackage[j].PackageStatus = Convert.ToInt32(AppEnums.ServiceStatus.ExpireService);
                                    if (objskill.PackageStatus != Convert.ToInt32(AppEnums.ServiceStatus.ExpireService))
                                    {
                                        removeflag = true;
                                    }
                                }
                                else if (ActivePackage[j].PackageName == AppConstant.Topweek.ToLower() || ActivePackage[j].PackageName.ToLower() == AppConstant.Topmonth.ToLower())
                                {
                                    ActivePackage[j].PackageStatus = Convert.ToInt32(AppEnums.ServiceStatus.ExpireService);
                                    if (lst[index].TopList == true)
                                    {
                                        objskill.TopList = false;
                                    }
                                    if (objskill.PackageStatus != Convert.ToInt32(AppEnums.ServiceStatus.ExpireService))
                                    {
                                        objskill.PackageStatus = Convert.ToInt32(AppEnums.ServiceStatus.ExpireService);
                                    }
                                }
                                else if (ActivePackage[j].PackageName == AppConstant.Oneweek.ToLower() || ActivePackage[j].PackageName.ToLower() == AppConstant.Onemonth.ToLower())
                                {
                                    ActivePackage[j].PackageStatus = Convert.ToInt32(AppEnums.ServiceStatus.ExpireService);

                                    if (objskill.PackageStatus != Convert.ToInt32(AppEnums.ServiceStatus.ExpireService))
                                    {
                                        removeflag = true;
                                    }
                                }

                                #region RemoveCollectionForImage
                                if (imguploaded > 0)
                                {
                                    objskill = await _skillrepository.RemoveImageAsync(objskill, ActivePackage[j].Id);
                                }
                                #endregion

                                #region UpdateCollectionForPackage
                                if (ActivePackage[j].PackageStatus == Convert.ToInt32(AppEnums.ServiceStatus.ExpireService))
                                {
                                    var package = await _repository.UpdatePackageInfoAsync(ActivePackage[j]);
                                }
                                #endregion 

                                #region UpdateCollectionForPackageStatus
                                if (removeflag == true)
                                    objskill.PackageStatus = Convert.ToInt32(AppEnums.ServiceStatus.ExpireService);
                                if (objskill.PackageStatus == Convert.ToInt32(AppEnums.ServiceStatus.ExpireService))
                                {
                                    objskill = await _skillrepository.UpdateSkillProfileAsync(objskill.Id, objskill);
                                }
                                #endregion
                            }
                        }
                    }
                    #endregion
                }
            }
            #endregion
        }
        return true;
    }
    #endregion
}