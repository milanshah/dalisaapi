using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Geolocation;

public class TopListService
{
    private readonly ISkillProfileRepository _repository;

    public TopListService(ISkillProfileRepository repository) { _repository = repository;}
    internal async Task<IEnumerable<SkillProfile>> GetAllTopListByUserID()
    {
        var LstTrending = await _repository.GetAllSkillProfiles();
        var result = LstTrending.Where(x => x.TopList == true).ToList();
        return result;
    }
}