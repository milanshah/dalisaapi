using System;
using System.Collections.Generic;
using System.Threading.Tasks;

public class SearchService
{
    private readonly ISearchRecordRepository _repository;

    public SearchService(ISearchRecordRepository repository) { _repository = repository; }

    internal async Task<SearchRecord> RecordSearchNear(double longitude, double latitude, string text) =>
        await _repository.RecordSearchNear(longitude, latitude, text);

    internal async Task<IEnumerable<SearchRecord>> GetRecentNearbySearches(double longitude, double latitude) =>
        await _repository.GetRecentSearchesNearby(longitude, latitude, TimeSpan.FromDays(7.0));
}