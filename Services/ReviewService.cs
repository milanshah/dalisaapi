using DaLiSaConnectApi.Model;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

public class ReviewService
{
    private readonly IReviewRepository _repository;
    private readonly IUserRepository _userRepository;
    public ReviewService(IReviewRepository repository, IUserRepository userRepository)
    {
        _repository = repository;
        _userRepository = userRepository;
    }

    //internal async Task<IEnumerable<ServiceReview>> GetReviewsForServiceAsync(SkillProfile service) => await _repository.GetReviewsForService(service.Id);

    internal async Task<IEnumerable<ReviewModel>> GetReviewsForServiceAsync(SkillProfile service)
    {
        var skillReviews = await _repository.GetReviewsForService(service.Id);
        //var result = new IEnumerable<ReviewModel>();
        var result = skillReviews.Select(x => new ReviewModel()
        {
            Id = x.Id,
            ServiceId = x.ServiceId,
            UserId = x.UserId,
            Comment = x.Comment,
            Rating = x.Rating,
            UserName = (_userRepository.TryGetUserProfileAsync(x.UserId).Result != null) ? _userRepository.TryGetUserProfileAsync(x.UserId).Result.DisplayName : "",
            CreateDateTime = (!string.IsNullOrEmpty(x.CreateDateTime)) ? x.CreateDateTime : "",
            UpdateDateTime = (!string.IsNullOrEmpty(x.UpdateDateTime)) ? x.UpdateDateTime : "",
        });

        return result;
    }
    internal async Task<ServiceReview?> TryGetUserReviewForServiceAsync(UserProfile user, SkillProfile service)
    {
        return await _repository.TryGetUserReviewForService(user.Id, service.Id);
    }

    internal async Task<ServiceReview> SetReviewForUserForService(UserProfile user, SkillProfile service, string comment, int rating)
    {
        var existing = await _repository.TryGetUserReviewForService(user.Id, service.Id);
        if (existing == null)
        {
            return await _repository.CreateReview(user.Id, service.Id, comment, rating);
        }
        else
        {
            var updated = new ServiceReview
            {
                Id = existing.Id,
                UserId = existing.UserId,
                ServiceId = existing.ServiceId,
                Comment = comment,
                Rating = rating,
                CreateDateTime = existing.CreateDateTime
            };

            return await _repository.UpdateReview(existing.Id, updated);
        }
    }
}