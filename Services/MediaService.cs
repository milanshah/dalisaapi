using System;
using Azure.Storage;
using Azure.Storage.Sas;
using Microsoft.Extensions.Options;

public class MediaService
{
    private readonly MediaStorageSettings _settings;

    public MediaService(IOptions<MediaStorageSettings> options) { _settings = options.Value; }

    internal string GenerateSasTokenForBlobCreation()
    {
        var builder = new BlobSasBuilder
        {
            BlobContainerName = _settings.ContainerName,
            Resource = "b",
            StartsOn = DateTime.Today,
            ExpiresOn = DateTime.UtcNow.AddMinutes(1),
            Protocol = SasProtocol.Https
        };
        builder.SetPermissions(BlobSasPermissions.Create);

        var token = builder.ToSasQueryParameters(new StorageSharedKeyCredential(_settings.AccountName, _settings.AccountKey));
        return token.ToString();
    }
}