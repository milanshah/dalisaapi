using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DaLiSaConnectApi.Model;
using Geolocation;

public class SkillProfileService
{
    private readonly ISkillProfileRepository _repository;

    private readonly ITrendingRecentIndustryRepository _TrendingRecentIndustryrepository;

    private readonly IIndustryRepository _IndustryRepositoryrepository;

    private readonly IReviewRepository _ReviewRepository;

    public SkillProfileService(ISkillProfileRepository repository, ITrendingRecentIndustryRepository TrendingRecentIndustryrepository, IIndustryRepository IndustryRepositoryrepository, IReviewRepository ReviewRepository)
    {
        _repository = repository;
        _TrendingRecentIndustryrepository = TrendingRecentIndustryrepository;
        _IndustryRepositoryrepository = IndustryRepositoryrepository;
        _ReviewRepository = ReviewRepository;
    }

    internal async Task<IEnumerable<SkillProfile>> GetAllSkillProfiles() => await _repository.GetAllSkillProfiles();

    internal async Task<IEnumerable<SkillProfile>> GetAllSkillProfilesNear(double latitude, double longitude, string searchtext, string userId, double distanceinMiles)
    {
        var profiles = await _repository.GetAllSkillProfiles();
        var results = new List<SkillProfile>();

        foreach (var profile in profiles)
        {
            var distance = GeoCalculator.GetDistance(
                latitude,
                longitude,
                profile.ContactInformation.GpsCoordinate?.Lat ?? 0.0,
                profile.ContactInformation.GpsCoordinate?.Long ?? 0.0);
            if (distance < distanceinMiles) results.Add(profile);
        }

        if (results.Count == 0)
        {
            foreach (var profile in profiles)
            {
                var distance = GeoCalculator.GetDistance(
                    latitude,
                    longitude,
                    profile.ContactInformation.GpsCoordinate?.Lat ?? 0.0,
                    profile.ContactInformation.GpsCoordinate?.Long ?? 0.0);
                if (distance < distanceinMiles) results.Add(profile);
            }
        }


        if (searchtext != null && searchtext != "")
        {
            var SearchItem = await _IndustryRepositoryrepository.GetIndustryName(searchtext);
            var lstTrendingArea = await _TrendingRecentIndustryrepository.GetCountofSearch(userId, searchtext);
            TrendingRecentIndustry trendingObj = new TrendingRecentIndustry();
            if (lstTrendingArea.Count == 0 && userId != null && userId != "")
            {
                if (SearchItem.Count == 0)// for new entry if the industry is not exist in db
                {
                    trendingObj.UserId = userId;
                    trendingObj.Seachcount = 1;
                    trendingObj.Datetime = DateTime.Today.ToString("dd/MM/yyyy");
                    trendingObj.Name = searchtext;
                    var ratval = await _TrendingRecentIndustryrepository.InsertTrendingRecentindustry(trendingObj, latitude, longitude);
                }
                else
                {
                    var lstIndustry = (from i in SearchItem
                                       select new TrendingRecentIndustry
                                       {
                                           UserId = userId,
                                           CategoryId = i.Id,
                                           CategoryName = i.Name,
                                           IndustryId = i.IndustryId,
                                           Seachcount = 1,
                                           Datetime = DateTime.Today.ToString("dd/MM/yyyy"),
                                           Name = searchtext
                                       });
                    var ratval = await _TrendingRecentIndustryrepository.InsertTrendingRecentindustry(lstIndustry.First(), latitude, longitude);
                }

            }
            else if (userId != null && userId != "")
            {
                var TrendingAreaId = (from T in lstTrendingArea
                                      select T.Id);
                var Isremoved = await _TrendingRecentIndustryrepository.RemoveOldSearch(TrendingAreaId.First());

                if (Isremoved)
                {
                    var lstIndustry = (from y in lstTrendingArea
                                       select new TrendingRecentIndustry
                                       {
                                           UserId = userId,
                                           CategoryId = y.CategoryId,
                                           CategoryName = y.CategoryName,
                                           IndustryId = y.IndustryId,
                                           Seachcount = y.Seachcount + 1,
                                           Datetime = DateTime.Today.ToString("dd/MM/yyyy"),
                                           Name = searchtext
                                       });
                    var ratval = await _TrendingRecentIndustryrepository.InsertTrendingRecentindustry(lstIndustry.First(), latitude, longitude);
                }
            }
        }


        return results;
    }

    internal async Task<SkillProfile?> TryGetSkillProfileByIdAsync(string id) => await _repository.TryGetSkillProfileByIdAsync(id);

    internal async Task<SkillProfile> SaveUpdatedSKillProfile(SkillProfile updated) => await _repository.UpdateSkillProfileAsync(updated.Id, updated);

    internal async Task<bool> RemoveSkillProfile(string id) => await _repository.RemoveSkillProfile(id);

    internal async Task<SkillProfile> CreateNewSkillProfile(SkillProfileForCreate request, UserProfile ownerProfile)
        => await _repository.InsertSkillProfile(request,
                                                ownerProfile.Id,
                                                MapMembershipLevelToPriorityLevel(ownerProfile.Membership.Level));

    private int MapMembershipLevelToPriorityLevel(MembershipInformation.MembershipLevel level)
    {
        switch (level)
        {
            case MembershipInformation.MembershipLevel.PREMIUMPLUS:
                return 30;
            case MembershipInformation.MembershipLevel.PREMIUM:
                return 20;
            case MembershipInformation.MembershipLevel.BASIC:
                return 10;
            case MembershipInformation.MembershipLevel.NONE:
            default:
                return int.MinValue;
        }
    }

    internal async Task<IEnumerable<SkillProfile>> GetServicesAsPerMiles(double latitude, double longitude, double distanceinMiles)
    {
        var profiles = await _repository.GetAllSkillProfiles();
        var results = new List<SkillProfile>();

        foreach (var profile in profiles)
        {
            var distance = GeoCalculator.GetDistance(
                latitude,
                longitude,
                profile.ContactInformation.GpsCoordinate?.Lat ?? 0.0,
                profile.ContactInformation.GpsCoordinate?.Long ?? 0.0);
            if (distance < distanceinMiles) results.Add(profile);
        }

        return results;
    }

    internal async Task<IEnumerable<SkillProfileModel>> GetAllSkillProfilesWithRatingAve(IEnumerable<SkillProfile> categories)
    {
        var results = new List<SkillProfileModel>();
        results = categories.Select(x => new SkillProfileModel()
        {
            Id = x.Id,
            Name = x.Name,
            Description = x.Description,
            IndustryCategoryId = x.IndustryCategoryId,
            IndustryId = x.IndustryId,
            UserId = x.UserId,
            ContactInformation = x.ContactInformation,
            Images = x.Images,
            WorkImages = x.WorkImages,
            Priority = x.Priority,
            PackageStatus = x.PackageStatus,
            TopList = x.TopList,
            Rating = CalculateRatingAverage(_ReviewRepository.GetReviewsForService(x.Id).Result)
        }).ToList();

        //foreach (var item in results)
        //{
        //    item.Rating = CalculateRatingAverage(_ReviewRepository.GetReviewsForService(item.Id).Result);
        //}

        return results;
    }

    private double CalculateRatingAverage(IList<ServiceReview> reviews)
    {
        double ratingAverage = 0;
        double ratingsSum = 0;
        double noOfRating = 0;

        if (reviews != null && reviews.Count > 0)
        {
            for (int i = 0; i < reviews.Count; i++)
            {
                ratingsSum += reviews[i].Rating;
                noOfRating += 1;
            }
            ratingAverage = ratingsSum / noOfRating;
        }
        return ratingAverage;
    }

}