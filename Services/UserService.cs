using System.Threading.Tasks;

public class UserService {
    private readonly IUserRepository _repository;

    public UserService(IUserRepository repository) => _repository = repository;

    public async Task<UserProfile> EnsureUserProfileAsync(string idClaim, string displayNameClaim)
    {
        var profile = await _repository.TryGetUserProfileFromClaimAsync(idClaim);
        if (profile == null)
            profile = await _repository.CreateNewUserProfileAsync(idClaim, displayNameClaim);

        return profile;
    }

    public async Task<UserProfile?> TryGetUserProfileAsync(string id) => await _repository.TryGetUserProfileAsync(id);

    public async Task<UserProfile> SaveUpdatedUserProfile(UserProfile updated) 
        => await _repository.UpdateUserProfileAsync(updated.Id, updated);

    public async Task<UserProfile> UpdateMembershipLevel(UserProfile profile, MembershipInformation.MembershipProvider provider, MembershipInformation.MembershipLevel level)
    {
        MembershipReader membershipReader;
        switch (provider) {
            case MembershipInformation.MembershipProvider.APPLE:
                membershipReader = new TrustingMembershipReader(); // TODO: new AppleMembershipReader();
                break;
            case MembershipInformation.MembershipProvider.GOOGLE:
                membershipReader = new TrustingMembershipReader(); // TODO  GoogleMembershipReader();
                break;
            default: 
                membershipReader = new NullMembershipReader();
                break;
        }

        var candidate = new UserProfile {
            Id = profile.Id,
            ADUserId = profile.ADUserId,
            DisplayName = profile.DisplayName,
            Membership = new MembershipInformation {
                Provider = provider,
                Level = membershipReader.CheckMembership(level) ?? profile.Membership.Level
            }
        };

        var updated = await _repository.UpdateUserProfileAsync(candidate.Id, candidate);

        return updated;
    }
}

interface MembershipReader
{
    MembershipInformation.MembershipLevel? CheckMembership(MembershipInformation.MembershipLevel level);
}

class AppleMembershipReader : MembershipReader
{
    public MembershipInformation.MembershipLevel? CheckMembership(MembershipInformation.MembershipLevel level)
    {
        throw new System.NotImplementedException();
    }
}

class GoogleMembershipReader : MembershipReader
{
    public MembershipInformation.MembershipLevel? CheckMembership(MembershipInformation.MembershipLevel level)
    {
        throw new System.NotImplementedException();
    }
}

class NullMembershipReader : MembershipReader
{
    public MembershipInformation.MembershipLevel? CheckMembership(MembershipInformation.MembershipLevel level) { return null; }
}

class TrustingMembershipReader : MembershipReader
{
    public MembershipInformation.MembershipLevel? CheckMembership(MembershipInformation.MembershipLevel level)
    {
        return level;
    }
}