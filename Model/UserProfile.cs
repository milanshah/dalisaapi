using System;
using System.ComponentModel.DataAnnotations;

public class HasKnownProviderAttribute : ValidationAttribute
{
    protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
    {
        if ((MembershipInformation.MembershipProvider)value! == MembershipInformation.MembershipProvider.UNKNOWN)
            return new ValidationResult("you must provide a known membership provider");
        
        return ValidationResult.Success;
    }
}

public class MembershipInformation
{
    public enum MembershipLevel { NONE, BASIC, PREMIUM, PREMIUMPLUS }

    public enum MembershipProvider { UNKNOWN, APPLE, GOOGLE }

    [Required]
    public MembershipLevel Level { get; init; } = MembershipLevel.NONE;

    [Required]
    [HasKnownProvider]
    public MembershipProvider Provider { get; init; } = MembershipProvider.UNKNOWN;
}

public class UserProfileResponse
{
    public string Id { get; init; } = null!;

    [Required]
    public string DisplayName { get; init; } = null!;

    [Required]
    public MembershipInformation Membership { get; init; } = null!;
}

public class UserProfile : UserProfileResponse {
    public string ADUserId { get; init; } = null!;
}