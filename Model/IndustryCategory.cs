using System.Collections.Generic;

public class Industry {
    public string Id { get; set; } = "";

    public string Name { get; set; } = "";
}

public class IndustryCategory {
    public string Id { get; set; } = "";

    public string Name { get; set; } = "";

    public string Image { get; set; } = "";

    public IList<Industry> Industries { get; set; } = new List<Industry>();
}

public class IndustryCategoryAndIndustry
{
    public string Id { get; set; } = "";

    public string Name { get; set; } = "";

    public string Image { get; set; } = "";

    public string IndustryId { get; set; } = "";

    public string IndustryName { get; set; } = "";
}