public class SearchRecord
{
    public double Longitude { get; init; }
    public double Latitude { get; init; }
    public string Text { get; init; } = null!;
}