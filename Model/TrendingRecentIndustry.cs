using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;



public class TrendingRecentIndustry {
    [Required]
    public string Id { get; init; } = null!;
    
    public string CategoryId { get; init; } = ""; 
    
    public string CategoryName { get; init; } = "";

    public string IndustryId { get; init; } = "";
 
    [Required]
    public string UserId { get; set; } = "";
    
    [Required]
    public long Seachcount { get; set; } = 0!;
    
    [Required]
    public string Datetime { get; set; } = null!;

    [Required]
    public double Latitude { get; init; } = 0.0!;

    [Required]
    public double Longitude { get; init; } = 0.0!;

    [Required]
    public string Name { get; set; } = "";

}

