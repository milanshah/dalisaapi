public class PackageDetails
{
    public string UserID { get; set; } = null!;
    public string ServiceID { get; set; } = null!;
    public string MasterPackageID { get; set; } = "";
    public string PackageName { get; set; } = "";
    public int ImageCount { get; set; } = 0;
    public string ExpireTime { get; set; } = "";
    public int PackageStatus { get; set; } = 0;
    public string CreateDateTime { get; set; } = "";
    public int TotalImageCount { get; set; } = 0;
    public string ServiceName { get; set; } = "";
}


public class PackageInfo : PackageDetails
{
    public string Id { get; init; } = null!;
}


public class Packages {

    public string Id { get; init; } = null!;
    public string PackageName { get; init; } = null!;
    public string PackageType { get; init; } = null!;
    public string Price { get; init; } = null!;
    public string ExpireDays { get; init; } = null!;
}

public class TotalCountImage 
{
    public string lastPackegeId { get; set; } = "";
    public string lastPackegeName { get; set; } = "";
    public int TotalPurchasedCount { get; set; } = 0;
    public int TotalUploadedImageCount { get; set; } = 0;
}

