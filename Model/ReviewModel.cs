﻿namespace DaLiSaConnectApi.Model
{
    public class ReviewModel
    {
        public string Id { get; init; } = null!;
        public string ServiceId { get; init; } = null!;
        public string UserId { get; init; } = null!;
        public string UserName { get;init; } = null!;
        public string Comment { get; init; } = null!;
        public int Rating { get; init; } = 0;
        public string CreateDateTime { get; init; } = null!;
        public string UpdateDateTime { get; init; } = null!;
    }
}
