using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

public class GpsCoordinate {
    [Required]
    public double Lat { get; init; }

    [Required]
    public double Long { get; init; }
}

public class ContactInformation {
    [Required]
    public string Name { get; init; } = null!;

    [Required]
    public string Email { get; init; } = null!;

    [Required]
    public string Phone { get; init; } = null!;

    public GpsCoordinate? GpsCoordinate { get; init; }
}

public class SkillProfileForCreate {
    [Required]
    public string Name { get; init; } = null!;

    [Required]
    public string Description { get; init; } = null!;

    [Required]
    public string IndustryCategoryId { get; init; } = null!;

    [Required]
    public string IndustryId { get; init; } = null!;

    [Required]
    public ContactInformation ContactInformation { get; init; } = null!;

    public List<string> Images { get; set; } = new List<string>();

    public List<WorkImage> WorkImages { get; set; } = new List<WorkImage>();

    public int PackageStatus { get; set; } = 0;
    public bool TopList { get; set; } = false;
}

public class SkillProfile : SkillProfileForCreate
{
    [Required]
    public string Id { get; init; } = null!;

    [Required]
    public string UserId { get; init; } = null!;

    [Required]
    public int Priority { get; init; } = 10;
}

public class WorkImage
{
    public string PackageID { get; set; } = "";

    public string Url { get; set; } = "";
}

public class UpdateImage
{
    public string ServiceID { get; set; } = "";
    public string OldImageURL { get; set; } = "";
    public string NewImageURL { get; set; } = "";
    public bool IsWorkImage { get; set; } = false;
}