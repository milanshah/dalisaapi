﻿namespace DaLiSaConnectApi.Model
{
    public class SkillProfileModel : SkillProfile
    {
        public double Rating { get; set; }
    }
}
