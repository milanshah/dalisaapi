using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

[ApiController]
[Route("api/searches")]
public class SearchesController : ControllerBase
{
    public class RegisterSearchData
    {
        [Required]
        public double? Longitude { get; init; } = null!;

        [Required]
        public double? Latitude { get; init; } = null!;

        [Required]
        [MinLength(1)]
        public string Text { get; init; } = null!;
    }

    private readonly SearchService _service;
    private readonly UserService _userService;

    public SearchesController(SearchService service, UserService userService) 
    {
         _service = service;
         _userService = userService;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<string>>> NearbySearches(
        [FromQuery(Name = "longitude")] double? longitude = null,
        [FromQuery(Name = "latitude")] double? latitude = null)
    {
        var result = (longitude != null && latitude != null) ?
            await _service.GetRecentNearbySearches(longitude ?? 0.0, latitude ?? 0.0) :
            new List<SearchRecord>();
        
        return Ok(result);
    }

    [Authorize]
    [HttpPost]
    public async Task<ActionResult> RegisterSearch([FromBody] RegisterSearchData data)
    {
        var myId = User.TryGetObjectId();
        var myDisplayName = User.Identity?.Name;
        if (myId == null || myDisplayName == null) return Forbid();
        var profile = await _userService.EnsureUserProfileAsync(myId, myDisplayName);

        var result = await _service.RecordSearchNear(data.Longitude ?? 0.0, data.Latitude ?? 0.0, data.Text);
        return Ok(result);
    }
}