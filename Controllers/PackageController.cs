using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using DaLiSaConnectApi.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


[ApiController]
[Route("api/package")]
public class PackageController : ControllerBase
{
    private readonly PackageService _service;
    private readonly PackageDetailService _packagedetailservice;
    private readonly SkillProfileService _skillservice;
    private readonly UserService _userService;


    public PackageController(PackageService service, PackageDetailService packagedetailservice, SkillProfileService skillservice, UserService userService) 
    {
        _service = service;
        _packagedetailservice = packagedetailservice;
        _skillservice = skillservice;
       _userService = userService;
    }

    [Authorize]
    [HttpPost]
    public async Task<ActionResult<IEnumerable<PackageInfo>>> SavePackage([FromBody] PackageDetails Data)
    {
        var myId = User.TryGetObjectId();
        var myDisplayName = User.Identity?.Name;
        if (myId == null || myDisplayName == null) return Forbid();

        //////////// START CHECK USER PACKAGE EXIST OR NOT //////////////
        var UserPackages = await _service.GetPackageList();
        var Packageservice = (from p in UserPackages
                              where p.ServiceID == Data.ServiceID
                              select p).ToList();
        //////////// END CHECK USER PACKAGE EXIST OR NOT ///////////////
        
        ///////////// START GET ALL PACKAGE TYPES //////////////////////  
        var packages = await _packagedetailservice.GetPackages();

        var packageDetails = (from x in packages
                              where x.PackageName.ToLower() == Data.PackageName.ToLower()
                              select x).ToList();
        ///////////// END GET ALL PACKAGE TYPES //////////////////////  

        var Days = packageDetails.Select(y => y.ExpireDays).First();

        SkillProfile objskill = new SkillProfile();
        PackageInfo objPackage = new PackageInfo();
        var newProfile = await _skillservice.TryGetSkillProfileByIdAsync(Data.ServiceID);
        if (newProfile == null) return NotFound();
        var totalImages = 0;

        #region ForTotalImageCount
        if (Data.PackageName.ToLower() == AppConstant.Oneimage.ToLower() || Data.PackageName.ToLower() == AppConstant.Tenimage.ToLower() || Data.PackageName.ToLower() == AppConstant.Oneweek.ToLower() || Data.PackageName.ToLower() == AppConstant.Onemonth.ToLower())
        {
            var ActivePackage = Packageservice.Where(p => p.PackageStatus != Convert.ToInt32(AppEnums.ServiceStatus.ExpireService)).ToList();
            if (ActivePackage.Count > 0)
            {
                for (int index = 0; index < ActivePackage.Count; index++)
                {
                    var imgcount = ActivePackage[index].ImageCount;
                    if (imgcount != 0)
                    {
                        if (imgcount == 1)
                        {
                            totalImages = totalImages == 0 ? Data.ImageCount + imgcount : totalImages + imgcount;
                        }
                        else
                        {
                            totalImages = imgcount;
                        }
                    }
                }
            }
            else
            {
                totalImages = Data.ImageCount;
            }
        }
        #endregion

        #region ForSaveAndUpdatePackage
        if (Data.PackageName.ToLower() == AppConstant.Oneimage.ToLower() || Data.PackageName.ToLower() == AppConstant.Tenimage.ToLower())
        {
            Data.PackageStatus = Convert.ToInt32(AppEnums.ServiceStatus.NewOrOngoingService);
            newProfile.PackageStatus = Convert.ToInt32(AppEnums.ServiceStatus.NewOrOngoingService);
        }
        else if (Data.PackageName.ToLower() == AppConstant.Topweek.ToLower() || Data.PackageName.ToLower() == AppConstant.Topmonth.ToLower())
        {
            Data.PackageStatus = Convert.ToInt32(AppEnums.ServiceStatus.TopListService);
            newProfile.PackageStatus = Convert.ToInt32(AppEnums.ServiceStatus.TopListService);
            newProfile.TopList = true;
        }
        else
        {
            Data.PackageName = Data.ImageCount == 1 ? AppConstant.Oneimage : AppConstant.Tenimage;
            Data.PackageStatus = Convert.ToInt32(AppEnums.ServiceStatus.RenewService);
            newProfile.PackageStatus = Convert.ToInt32(AppEnums.ServiceStatus.RenewService);
        }
        #endregion

        Data.ServiceName = newProfile.Name;
        Data.ExpireTime = Convert.ToString(DateTime.UtcNow + TimeSpan.FromDays(Convert.ToInt32(Days)));
        Data.MasterPackageID = packageDetails.Select(y => y.Id).First();
        if (totalImages == 10 || totalImages < 10)
        {
            Data.TotalImageCount = totalImages;
        }
        objPackage = await _service.StorePackageInfo(Data);
        objskill = await _skillservice.SaveUpdatedSKillProfile(newProfile);

        
        return Ok(objPackage);
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<TotalCountImage>> GetTotalImageCountByServiceId([FromRoute] string id)
    {
        var UserPackages = await _service.GetPackageList();
        var item = (from p in UserPackages
                              where p.ServiceID == id
                              select p).ToList();

        TotalCountImage objResult = new TotalCountImage();
        for (int index = 0; index < item.Count; index++)
        {
            if (item[index].PackageStatus != Convert.ToInt32(AppEnums.ServiceStatus.ExpireService))
            {
                var getProfile = await _skillservice.TryGetSkillProfileByIdAsync(id);
                if (getProfile == null) return NotFound();
                var imguploaded = getProfile.WorkImages.Where(x => x.PackageID == item[index].Id).Count();
                if (item[index].PackageName.ToLower() == AppConstant.Oneimage.ToLower())
                {
                    if (imguploaded > 0)
                    {
                        objResult.TotalUploadedImageCount += imguploaded;
                    }
                    else
                    {
                        objResult.lastPackegeId = item[index].Id;
                    }
                    objResult.TotalPurchasedCount += item[index].ImageCount;
                }
                else if (item[index].PackageName.ToLower() == AppConstant.Tenimage.ToLower())
                {
                    objResult.TotalUploadedImageCount = imguploaded > 0 ? imguploaded : 0;
                    objResult.lastPackegeId = item[index].Id;
                    objResult.TotalPurchasedCount = item[index].ImageCount;
                }
                else if (item[index].PackageName.ToLower() == AppConstant.Oneweek.ToLower() || item[index].PackageName.ToLower() == AppConstant.Onemonth.ToLower())
                {
                    var imgcount = item[index].ImageCount;
                    if (imgcount == 1)
                    {
                        if (imguploaded > 0)
                        {
                            objResult.TotalUploadedImageCount += imguploaded;
                        }
                        else
                        {
                            objResult.lastPackegeId = item[index].Id;
                        }
                        objResult.TotalPurchasedCount += item[index].ImageCount;
                    }
                    else
                    {
                        objResult.TotalUploadedImageCount = imguploaded > 0 ? imguploaded : 0;
                        objResult.lastPackegeId = item[index].Id;
                        objResult.TotalPurchasedCount = item[index].ImageCount;
                    }
                }
                objResult.lastPackegeName = item[index].PackageName;
            }
        }

        return Ok(objResult);
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<PackageInfo>>> GetPackgeDetailsById([FromQuery(Name = "id")] string? id = null,
                                                                                   [FromQuery(Name = "userId")] string? userId = null)
    {
            if (userId != null)
            {
                var retval = await _service.TryExpireByUserId(userId ?? "");
            }

            var UserPackages = await _service.GetPackageList();
            var result = new List<PackageInfo>();
            result = (from p in UserPackages
                            where p.ServiceID == id
                            select p).ToList();

        return Ok(result);
    }
}