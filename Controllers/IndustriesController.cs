using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

[ApiController]
[Route("api/industrycategories")]
public class IndustriesController: ControllerBase {
    private readonly IndustryService _service;

    public IndustriesController(IndustryService service) { _service = service; }

    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<IEnumerable<IndustryCategory>>> List()
    {
        var categories = await _service.GetAllIndustryCategoriesAsync();
        return Ok(categories);
    }
}