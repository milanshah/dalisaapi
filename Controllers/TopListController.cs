using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DaLiSaConnectApi.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

[ApiController]
[Route("api/toplist")]
public class TopListController : ControllerBase {
    private readonly TopListService _service;
    private readonly PackageService _packageservice;
    private readonly SkillProfileService _skillservice;

    public TopListController(TopListService service, PackageService packageservice, SkillProfileService skillservice) 
    { 
        _service = service;
        _packageservice = packageservice;
        _skillservice = skillservice;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<SkillProfile>>> GetTopList()
    {
        var categories = await _service.GetAllTopListByUserID();
        return Ok(categories);
    }
}