using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

[ApiController]
[Route("api/users")]
public class UserController : ControllerBase
{
    private readonly UserService _service;

    public UserController(UserService service) => _service = service;

    [Authorize]
    [HttpGet("me")]
    public async Task<ActionResult<UserProfileResponse>> Me()
    {
        var myId = User.TryGetObjectId();
        var myDisplayName = User.Identity?.Name;
        if (myId == null || myDisplayName == null) return NotFound();

        var profile = await _service.EnsureUserProfileAsync(myId, myDisplayName);
        return Ok(RestrictProfile(profile));
    }

    //[Authorize]
    //[HttpPut("me/membership")]
    //public async Task<ActionResult<UserProfileResponse>> UpdateMembership([FromBody] MembershipInformation membership)
    //{
    //    var myId = User.TryGetObjectId();
    //    var myDisplayName = User.Identity?.Name;
    //    if (myId == null || myDisplayName == null) return Forbid();
    //    var myProfile = await _service.EnsureUserProfileAsync(myId, myDisplayName);

    //    var updatedProfile = await _service.UpdateMembershipLevel(myProfile, membership.Provider, membership.Level);

    //    return Ok(RestrictProfile(updatedProfile));
    //}

    // [HttpGet("{id}")]
    // public async Task<ActionResult<UserProfileResponse>> Single([FromRoute] string id)
    // {
    //     if (!id.IsHex()) return NotFound();

    //     var profile = await _service.TryGetUserProfileAsync(id);
    //     if (profile == null) return NotFound();

    //     return Ok(RestrictProfile(profile));
    // }

    // [Authorize]
    // [HttpPatch("me")]
    // public async Task<ActionResult<UserProfileResponse>> Update([FromBody] JsonPatchDocument<UserProfile> patches)
    // {
    //     var myId = User.TryGetObjectId();
    //     var myFirstName = User.TryGetFirstName();
    //     var myLastName = User.TryGetLastName();
    //     if (myId == null || myFirstName == null || myLastName == null) return NotFound();

    //     var profile = await _service.EnsureUserProfileAsync(myId, myFirstName, myLastName);

    //     patches.PreventPatchingProperties(new[] { nameof(UserProfile.Id), nameof(UserProfile.ADUserId) }, ModelState);
    //     patches.ApplyTo(profile, ModelState);
    //     if (!ModelState.IsValid || !TryValidateModel(profile, nameof(UserProfile))) return BadRequest(ModelState);

    //     var updated = await _service.SaveUpdatedUserProfile(profile);
    //     return Ok(RestrictProfile(updated));
    // }

    private UserProfileResponse RestrictProfile(UserProfile profile) => new UserProfileResponse
    {
        Id = profile.Id,
        DisplayName = profile.DisplayName,
        Membership = profile.Membership
    };
}