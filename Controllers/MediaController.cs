using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

public class MediaToken
{
    public string Token { get; set; } = "";
}

[ApiController]
[Route("api/media")]
public class MediaController : ControllerBase
{
    private readonly MediaService _service;

    public MediaController(MediaService service) { _service = service; }

    [Authorize]
    [HttpGet("token")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public ActionResult<MediaToken> AcquireToken()
    {
        var token = _service.GenerateSasTokenForBlobCreation();
        return Ok(new MediaToken { Token = token });
    }
}