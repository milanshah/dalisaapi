using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using DaLiSaConnectApi.Helpers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

[ApiController]
[Route("api/log")]
public class LogFileController : ControllerBase {
    private readonly UserService _userService;
    private readonly IHostingEnvironment _environment;
    public IConfiguration _configuration { get; }

    public LogFileController(UserService userService, IHostingEnvironment environment, IConfiguration configuration)
    {
        _userService = userService;
        _environment = environment;
        _configuration = configuration;
    }

    [HttpPost()]
    public async Task<IActionResult> UploadFile(List<IFormFile> files)
    {
        long size = files.Sum(f => f.Length);
        var filename = files.Select(x => x.FileName);

        //var rootpath = _hostingEnvironment.WebRootPath;
        //var rootpath = AppContext.BaseDirectory.Substring(0, AppContext.BaseDirectory.IndexOf("bin"));
        //var rootpath = Directory.GetCurrentDirectory();
        string directory = string.Empty;
        directory = _environment.WebRootPath + @"\" + _configuration["FileUploadDirectory:LogFile"];
        DirectoryInfo dir1;
        dir1 = new DirectoryInfo(directory);
        if (!dir1.Exists)
            dir1.Create();

        // full path to file in temp location
        var filePath = Path.Combine(directory, filename.First());

        foreach (var formFile in files)
        {
            if (formFile.Length > 0)
            {
                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await formFile.CopyToAsync(stream);
                }
            }
        }


        return Ok(new { count = files.Count, size, filePath });
    }
}
