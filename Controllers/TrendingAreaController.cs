using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

[ApiController]
[Route("api/TrendingArea")]
public class TrendingAreaController : ControllerBase
{
    private readonly TrendingRecentService _service;
    private readonly PackageService _packageservice;
    private readonly UserService _userService;

    public TrendingAreaController(TrendingRecentService service, PackageService packageservice, UserService userService) 
    {
        _service = service;
        _userService = userService;
        _packageservice = packageservice;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<TrendingRecentIndustry>>> GetAllTrendingArea(
        [FromQuery(Name = "latitude")] double? latitude = null,
        [FromQuery(Name = "longitude")] double? longitude = null,
        [FromQuery(Name = "userId")] string? userId = null,
        [FromQuery(Name = "IsTrendingArea")] bool? IsTrendingArea = false)
    {
        IEnumerable<TrendingRecentIndustry> TrendingArea = new List<TrendingRecentIndustry>();
        if (IsTrendingArea == true)
        {
            TrendingArea =
             await _service.GetAllTrendingNearby(latitude ?? 0.0, longitude ?? 0.0);
        }
        else
        {
            TrendingArea = await _service.GetAllRecentSearch(userId ?? "");
        }
        
        if (userId != null)
        {
            var retval = await _packageservice.TryExpireByUserId(userId ?? "");
        }

        return Ok(TrendingArea);
    }

}