using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using DaLiSaConnectApi.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

[ApiController]
[Route("api/services/{serviceId}/reviews")]
public class ReviewsController :  ControllerBase 
{
    private readonly SkillProfileService _skillProfileService;
    private readonly ReviewService _reviewService;
    private readonly UserService _userService;

    public ReviewsController(SkillProfileService service, ReviewService reviewService, UserService userService)
    {
        _skillProfileService = service;
        _reviewService = reviewService;
        _userService = userService;
    }

    //[HttpGet]
    //public async Task<ActionResult<IEnumerable<ServiceReview>>> ReviewsForService([FromRoute] string serviceId)
    //{
    //    if (!serviceId.IsHex()) return NotFound();

    //    var service = await _skillProfileService.TryGetSkillProfileByIdAsync(serviceId);
    //    if (service == null) return NotFound();

    //    var reviews = await _reviewService.GetReviewsForServiceAsync(service);
    //    return Ok(reviews);
    //}

    [HttpGet]
    public async Task<ActionResult<IEnumerable<ReviewModel>>> ReviewsForService([FromRoute] string serviceId)
    {
        if (!serviceId.IsHex()) return NotFound();

        var service = await _skillProfileService.TryGetSkillProfileByIdAsync(serviceId);
        if (service == null) return NotFound();

        var reviews = await _reviewService.GetReviewsForServiceAsync(service);
        return Ok(reviews);
    }

    [Authorize]
    [HttpGet("mine")]
    public async Task<ActionResult<ServiceReview>> GetMyReview([FromRoute] string serviceId)
    {
        if (!serviceId.IsHex()) return NotFound();

        var service = await _skillProfileService.TryGetSkillProfileByIdAsync(serviceId);
        if (service == null) return NotFound();

        var myId = User.TryGetObjectId();
        var myName = User.Identity?.Name;
        if (myId == null || myName == null) return Forbid();
        var user = await _userService.EnsureUserProfileAsync(myId, myName);

        var result = await _reviewService.TryGetUserReviewForServiceAsync(user, service);
        if (result == null) return NotFound();
        else return Ok(result);
    }

    [Authorize]
    [HttpPut("mine")]
    public async Task<ActionResult> ReviewService([FromRoute] string serviceId, [FromBody] ServiceReviewForUpsert review)
    {
        if (!serviceId.IsHex()) return NotFound();

        var service = await _skillProfileService.TryGetSkillProfileByIdAsync(serviceId);
        if (service == null) return NotFound();

        var myId = User.TryGetObjectId();
        var myName = User.Identity?.Name;
        if (myId == null || myName == null) return Forbid();
        var user = await _userService.EnsureUserProfileAsync(myId, myName);

        var result = await _reviewService.SetReviewForUserForService(user, service, review.Comment, review.Rating);
        return Ok(result);
    }

    public class ServiceReviewForUpsert
    {
        [Required]
        [MinLength(1)]
        public string Comment { get; init; } = null!;

        [Required]
        [Range(minimum: 0, maximum: 5)]
        public int Rating { get; init; } = 0;
    }
}