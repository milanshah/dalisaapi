using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DaLiSaConnectApi.Helpers;
using DaLiSaConnectApi.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

[ApiController]
[Route("api/skills")]
public class SkillProfilesController : ControllerBase
{
    private readonly SkillProfileService _service;
    private readonly UserService _userService;
    private readonly PackageService _packageService;

    public SkillProfilesController(SkillProfileService service, UserService userService, PackageService packageService)
    {
        _service = service;
        _userService = userService;
        _packageService = packageService;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<SkillProfileModel>>> ListAll(
        [FromQuery(Name = "latitude")] double? latitude = null,
        [FromQuery(Name = "longitude")] double? longitude = null,
        [FromQuery(Name = "userId")] string? userId = null,
        [FromQuery(Name = "searchText")] string? searchText = null,
        [FromQuery(Name = "distanceinMiles")] double? distanceinMiles = null,
        [FromQuery(Name = "providerList")] bool? providerList = false)
    {
        //var contentpath = _hostingEnvironment.ContentRootPath;
        //var webpath = _hostingEnvironment.WebRootPath;
        //var webfilepath = _hostingEnvironment.WebRootFileProvider;

        //var path = AppContext.BaseDirectory.Substring(0, AppContext.BaseDirectory.IndexOf("bin"));
        //AppLog.ApplicationLog("============================== Get service List ==============================", path);

        if (userId != null)
        {
            var retval = await _packageService.TryExpireByUserId(userId ?? "");
        }

        //if (userId != null) // for visable all service.
        //{
        //    categories = categories.Where(it => it.UserId == userId);
        //}
        //else
        //{
        //    categories = await _service.GetServicesAsPerMiles(latitude ?? 0.0, longitude ?? 0.0, distanceinMiles ?? 1.0);
        //}
        IEnumerable<SkillProfile> categories = (latitude != null && longitude != null && distanceinMiles != null && userId != null) ?
            await _service.GetAllSkillProfilesNear(latitude ?? 0.0, longitude ?? 0.0, searchText ?? "", userId ?? "", distanceinMiles ?? 1.0) :
            await _service.GetAllSkillProfiles();

        //Add average rating for all service
        var result = await _service.GetAllSkillProfilesWithRatingAve(categories);

        if (userId != null && providerList == true)
        {
            //categories = categories.Where(it => it.UserId == userId);
            result = result.Where(it => it.UserId == userId).OrderByDescending(c => c.Id);
        }
        else
        {
            result = from s in result
                         //orderby s.TopList == true descending
                     orderby s.Id descending
                     select s;
        }

        return Ok(result);
    }

    [Authorize]
    [HttpPost]
    public async Task<ActionResult<SkillProfile>> Create([FromBody] SkillProfileForCreate data)
    {
        var myId = User.TryGetObjectId();
        var myDisplayName = User.Identity?.Name;
        if (myId == null || myDisplayName == null) return Forbid();

        var profile = await _userService.EnsureUserProfileAsync(myId, myDisplayName);
        // Any user can create service.
        //if (profile.Membership.Level == MembershipInformation.MembershipLevel.NONE) return Forbid();

        var result = await _service.CreateNewSkillProfile(data, profile);

        return Ok(result);
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<SkillProfile>> Single([FromRoute] string id)
    {
        if (!id.IsHex()) return NotFound();
        //var path = AppContext.BaseDirectory.Substring(0, AppContext.BaseDirectory.IndexOf("bin"));
        //AppLog.ApplicationLog("============================== Get service to upload file start ==============================", path);

        var item = await _service.TryGetSkillProfileByIdAsync(id);
        //AppLog.ApplicationLog("============================== Get service to upload file end ==============================", path);
        if (item == null) return NotFound();

        else return Ok(item);
    }

    [Authorize]
    [HttpPatch("{id}")]
    public async Task<ActionResult<SkillProfile>> UpdateSkillProfile([FromRoute] string id, [FromBody] JsonPatchDocument<SkillProfile> patches)
    {
        if (!id.IsHex()) return NotFound();

        var myId = User.TryGetObjectId();
        var myName = User.Identity?.Name;
        if (myId == null || myName == null) return Forbid();

        var profile = await _userService.EnsureUserProfileAsync(myId, myName);
        var skillProfile = await _service.TryGetSkillProfileByIdAsync(id);
        if (skillProfile == null) return NotFound();
        if (skillProfile.UserId != profile.Id) return Forbid();

        // var path = AppContext.BaseDirectory.Substring(0, AppContext.BaseDirectory.IndexOf("bin"));
        // AppLog.ApplicationLog("============================== File upload start  ==============================", path);

        patches.PreventPatchingProperties(new[] {
            nameof(SkillProfile.Id),
            nameof(SkillProfile.UserId),
            nameof(SkillProfile.Priority),
        }, ModelState);
        patches.ApplyTo(skillProfile, ModelState);
        if (!ModelState.IsValid || !TryValidateModel(skillProfile, nameof(SkillProfile))) return BadRequest(ModelState);

        var updated = await _service.SaveUpdatedSKillProfile(skillProfile);
        //AppLog.ApplicationLog("============================== File upload End  ==============================", path);
        return Ok(updated);
    }

    [Authorize]
    [HttpDelete("{id}")]
    public async Task<ActionResult<SkillProfile>> RemoveSkillProfile([FromRoute] string id)
    {
        if (!id.IsHex()) return NotFound();

        var idClaim = User.TryGetObjectId();
        var nameClaim = User.Identity?.Name;
        if (idClaim == null || nameClaim == null) return Forbid();

        var profile = await _userService.EnsureUserProfileAsync(idClaim, nameClaim);
        var service = await _service.TryGetSkillProfileByIdAsync(id);
        if (service == null) return NotFound();
        if (service.UserId != profile.Id) return Forbid();

        await _service.RemoveSkillProfile(id);
        return NoContent();
    }

    [Authorize]
    [HttpPut]
    public async Task<ActionResult<SkillProfile>> updateImage([FromBody] UpdateImage update)
    {
        if (!update.ServiceID.IsHex()) return NotFound();

        var idClaim = User.TryGetObjectId();
        var nameClaim = User.Identity?.Name;
        if (idClaim == null || nameClaim == null) return Forbid();

        var profile = await _userService.EnsureUserProfileAsync(idClaim, nameClaim);
        var service = await _service.TryGetSkillProfileByIdAsync(update.ServiceID);
        if (service == null) return NotFound();
        if (service.UserId != profile.Id) return Forbid();

        if (update.IsWorkImage == true)
        {
            foreach (var itemWorkImage in service.WorkImages.Where(w => w.Url.Contains(update.OldImageURL)))
            {
                itemWorkImage.Url = update.NewImageURL;
            }
        }
        else
        {
            for (int index = 0; index < service.Images.Count; index++)
            {
                if (service.Images[index].ToLower() == update.OldImageURL.ToLower())
                {
                    if (update.NewImageURL != null && update.NewImageURL != "")
                    {
                        service.Images[index] = update.NewImageURL;
                    }
                    else
                    {
                        service.Images.RemoveAt(index);
                    }
                }
            }
        }

        var item = await _service.SaveUpdatedSKillProfile(service);
        return Ok(item);
    }
}