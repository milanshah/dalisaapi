using System.IO;
using System.Linq;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.Identity.Web;

namespace DaLiSaConnectApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddMicrosoftIdentityWebApi(options =>
            {
                Configuration.Bind("AzureADB2C", options);
                options.TokenValidationParameters.NameClaimType = "name";
            }, options =>
            {
                Configuration.Bind("AzureADB2C", options);
            });

            services.Configure<MediaStorageSettings>(Configuration.GetSection(MediaStorageSettings.SectionName));
            var cosmosDBSettings = Configuration.GetSection(CosmosDBSettings.SectionName).Get<CosmosDBSettings>();

            services.AddControllers(options =>
            {
                var builder = new ServiceCollection()
                    .AddLogging()
                    .AddMvc()
                    .AddNewtonsoftJson()
                    .Services.BuildServiceProvider();

                var formatter = builder
                    .GetRequiredService<IOptions<MvcOptions>>()
                    .Value
                    .InputFormatters
                    .OfType<NewtonsoftJsonPatchInputFormatter>()
                    .First();

                options.InputFormatters.Insert(0, formatter);
            }).AddJsonOptions(opts =>
            {
                opts.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
            });

            // services.AddAuthorization(options => {
            //     options.AddPolicy("BasicAppUser", policy => policy.Requirements.Add(new ScopesRequirement("app.user.basic")));
            // });

            services.AddSingleton<MongoDatabaseContext>(services =>
                new MongoDatabaseContext(cosmosDBSettings.ConnectionUrl, cosmosDBSettings.DatabaseName));

            services.AddScoped<IUserRepository, MongoUserRepository>(services =>
            {
                var context = services.GetService<MongoDatabaseContext>()!;
                return new MongoUserRepository(context, "users");
            });
            services.AddScoped<IIndustryRepository, MongoIndustryRepository>(services =>
            {
                var context = services.GetService<MongoDatabaseContext>()!;
                return new MongoIndustryRepository(context, "industryCategories");
            });
            services.AddScoped<ISkillProfileRepository, MongoSkillProfileRepository>(services =>
            {
                var context = services.GetService<MongoDatabaseContext>()!;
                return new MongoSkillProfileRepository(context, "skillProfiles");
            });
            services.AddScoped<IReviewRepository, MongoReviewRepository>(services =>
            {
                var context = services.GetService<MongoDatabaseContext>()!;
                return new MongoReviewRepository(context, "reviews");
            });
            services.AddScoped<ISearchRecordRepository, MongoSearchRecordRepository>(services =>
            {
                var context = services.GetService<MongoDatabaseContext>()!;
                return new MongoSearchRecordRepository(context, "searches");
            });
            services.AddScoped<ITrendingRecentIndustryRepository, MongoTrendingRecentIndustryRepository>(services => {
                var context = services.GetService<MongoDatabaseContext>()!;
                return new MongoTrendingRecentIndustryRepository(context, "trendingRecentIndustry");
            });
            services.AddScoped<IPackageDetailRepository, MongoPackageDetailsRepository>(services =>
            {
                var context = services.GetService<MongoDatabaseContext>()!;
                return new MongoPackageDetailsRepository(context, "packageTypes");
            });
            services.AddScoped<IPackageRepository, MongoPackageRepository>(services => {
                var context = services.GetService<MongoDatabaseContext>()!;
                return new MongoPackageRepository(context, "packageInfo");
            });

            services.AddTransient<IndustryService>();
            services.AddTransient<SkillProfileService>();
            services.AddTransient<MediaService>();
            services.AddTransient<UserService>();
            services.AddTransient<ReviewService>();
            services.AddTransient<SearchService>();
            services.AddTransient<TrendingRecentService>();
            services.AddTransient<PackageService>();
            services.AddTransient<PackageDetailService>();
            services.AddTransient<TopListService>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();
            //app.UseStaticFiles();
            //app.UseDirectoryBrowser(new DirectoryBrowserOptions
            //{
            //    FileProvider = new PhysicalFileProvider(
            //     Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "LogFile")),
            //    RequestPath = "/LogFile"
            //});
            

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}