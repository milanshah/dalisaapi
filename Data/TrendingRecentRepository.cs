using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;

public interface ITrendingRecentIndustryRepository
{
    Task<bool> InsertTrendingRecentindustry(TrendingRecentIndustry request, double latitude, double longitude);

    Task<IList<TrendingRecentIndustry>> GetAllTrending();

    Task<IList<TrendingRecentIndustry>> GetCountofSearch(string UserID,string searchtext);

    //Task<bool> UpdateTrendingRecentindustry(TrendingRecentIndustry request, double latitude, double longitude);

    Task<bool> RemoveOldSearch(string id);
}

internal class MongoTrendingRecentIndustryRepository : ITrendingRecentIndustryRepository
{
    private class BsonTrendingRecentIndustry
    {
        [BsonId]
        public BsonObjectId Id { get; set; } = BsonObjectId.Empty;

        [BsonElement("categoryId")]
        public BsonString categoryId { get; set; } = BsonString.Empty;
        
        [BsonElement("categoryName")]
        public BsonString categoryName { get; set; } = BsonString.Empty;

        [BsonElement("industryId")]
        public BsonString industryId { get; set; } = BsonString.Empty;

        [BsonElement("userId")]
        public BsonString userId { get; set; } = BsonString.Empty;

        [BsonElement("name")]
        public BsonString name { get; set; } = BsonString.Empty;

        [BsonElement("seachcount")]
        public BsonInt64 seachCount { get; set; } = 0;

        [BsonElement("searchDate")]
        public BsonString searchDate { get; set; } = BsonString.Empty;

        [BsonElement("latitude")]
        public BsonDouble latitude { get; set; } = new BsonDouble(0.0);

        [BsonElement("longitude")]
        public BsonDouble longitude { get; set; } = new BsonDouble(0.0);

        public static BsonTrendingRecentIndustry FromCreateTrendingRequest(TrendingRecentIndustry request, double latitude, double longitude, bool withNewId = true) => new BsonTrendingRecentIndustry
        {

            Id = withNewId ? new BsonObjectId(new ObjectId()) : new BsonObjectId(new ObjectId(request.Id)),
            userId = new BsonString(request.UserId),
            categoryId = new BsonString(request.CategoryId),
            categoryName = new BsonString(request.CategoryName),
            industryId = new BsonString(request.IndustryId),
            name = new BsonString(request.Name),
            seachCount = new BsonInt64(request.Seachcount),
            searchDate = new BsonString(request.Datetime),
            latitude = new BsonDouble(latitude),
            longitude = new BsonDouble(longitude)
        };

        [BsonIgnore]
        public TrendingRecentIndustry AsTrendingRecentIndustry => new TrendingRecentIndustry
        {
            Id = this.Id.AsObjectId.ToString(),
            UserId = this.userId.AsString,
            CategoryId = this.categoryId.AsString,
            CategoryName = this.categoryName.AsString,
            IndustryId = this.industryId.AsString,
            Name = this.name.AsString,
            Seachcount = this.seachCount.AsInt64,
            Datetime = this.searchDate.AsString,
            Latitude = this.latitude.AsDouble,
            Longitude = this.longitude.AsDouble
        };
    }

    private readonly MongoDatabaseContext _context;


    private readonly IMongoCollection<BsonTrendingRecentIndustry> _collection;

    internal MongoTrendingRecentIndustryRepository(MongoDatabaseContext context, string collectionName)
    {
        _context = context;
        _collection = _context.Database.GetCollection<BsonTrendingRecentIndustry>(collectionName);
    }

    public async Task<bool> InsertTrendingRecentindustry(TrendingRecentIndustry request, double latitude, double longitude)
    {
        var document = BsonTrendingRecentIndustry.FromCreateTrendingRequest(request, latitude, longitude);
        await _collection.InsertOneAsync(document);
        return true;
    }

    public async Task<IList<TrendingRecentIndustry>> GetAllTrending() {

        var filter = Builders<BsonTrendingRecentIndustry>.Filter.Empty;
        var result = await _collection.FindAsync(filter);
        return result.ToEnumerable().Select(item => item.AsTrendingRecentIndustry).ToList();
    }

    public async Task<IList<TrendingRecentIndustry>> GetCountofSearch(string UserID,string searchtext)
    {
        var filter = Builders<BsonTrendingRecentIndustry>.Filter.Empty;
        var result = await _collection.FindAsync(filter);
        var LstTrendingService = result.ToEnumerable().Select(item => item.AsTrendingRecentIndustry).ToList();
        List<TrendingRecentIndustry> results = new List<TrendingRecentIndustry>();
        if (LstTrendingService.Count > 0)
        {
            results = (from list in LstTrendingService
                       where list.UserId == UserID && list.Name.ToLower() == searchtext.ToLower()
                       select list).ToList();
        }

        return results;
    }

    //public async Task<bool> UpdateTrendingRecentindustry(TrendingRecentIndustry request, double latitude, double longitude)
    //{
    //    var filter = Builders<BsonTrendingRecentIndustry>.Filter.IdFilter(request.Id);
    //    var document = BsonTrendingRecentIndustry.FromCreateTrendingRequest(request, latitude, longitude,false);
    //    var result = await _collection.FindOneAndReplaceAsync(filter, document, new FindOneAndReplaceOptions<BsonTrendingRecentIndustry>
    //    {
    //        ReturnDocument = ReturnDocument.After
    //    });
    //    var Ratval = result != null ? true : false;
    //    return Ratval;
    //}

    public async Task<bool> RemoveOldSearch(string id)
    {
        var filter = Builders<BsonTrendingRecentIndustry>.Filter.IdFilter(id);
        var result = await _collection.DeleteOneAsync(filter);
        return result.DeletedCount == 1;
    }

}