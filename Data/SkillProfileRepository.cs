using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;

public interface ISkillProfileRepository {
    Task<IList<SkillProfile>> GetAllSkillProfiles();

    Task<SkillProfile?> TryGetSkillProfileByIdAsync(string id);

    Task<SkillProfile> UpdateSkillProfileAsync(string id, SkillProfile updated);
    Task<SkillProfile> RemoveImageAsync(SkillProfile updated,string packageID);

    Task<bool> RemoveSkillProfile(string id);

    Task<SkillProfile> InsertSkillProfile(SkillProfileForCreate request, string ownerId, int priority);

    Task<IList<SkillProfile>> TryGetListSkillByUserIdAsync(string id);
}

internal class MongoSkillProfileRepository : ISkillProfileRepository
{
    [BsonNoId]
    public class BsonGpsCoordinate
    {
        [BsonElement("lat")]
        public BsonDouble Lat { get; set; } = new BsonDouble(0.0);

        [BsonElement("long")]
        public BsonDouble Long { get; set; } = new BsonDouble(0.0);

        [BsonIgnore]
        public GpsCoordinate AsAppGpsCoordinate => new GpsCoordinate
        {
            Lat = this.Lat.AsDouble,
            Long = this.Long.AsDouble
        };

        public static BsonGpsCoordinate CreateFromAppGpsCoordinate(GpsCoordinate source) => new BsonGpsCoordinate
        {
            Lat = new BsonDouble(source.Lat),
            Long = new BsonDouble(source.Long)
        };
    }

    [BsonNoId]
    private class BsonContactInformation
    {
        [BsonElement("name")]
        public BsonString Name { get; set; } = BsonString.Empty;

        [BsonElement("email")]
        public BsonString Email { get; set; } = BsonString.Empty;

        [BsonElement("phone")]
        public BsonString Phone { get; set; } = BsonString.Empty;

        [BsonElement("gpsCoordinate")]
        public BsonValue GpsCoordinate { get; set; } = BsonNull.Value; // BsonDocument

        [BsonIgnore]
        public ContactInformation AsAppContactInformation => new ContactInformation
        {
            Name = this.Name.AsString,
            Email = this.Email.AsString,
            Phone = this.Phone.AsString,
            GpsCoordinate = this.GpsCoordinate.BsonType == BsonType.Document ?
                BsonSerializer.Deserialize<BsonGpsCoordinate>(this.GpsCoordinate.AsBsonDocument).AsAppGpsCoordinate :
                null
        };

        public static BsonContactInformation CreateFromAppContactInformation(ContactInformation contactInformation) => new BsonContactInformation
        {
            Name = new BsonString(contactInformation.Name),
            Email = new BsonString(contactInformation.Email),
            Phone = new BsonString(contactInformation.Phone),
            GpsCoordinate = contactInformation.GpsCoordinate != null ?
                BsonDocumentWrapper.Create<BsonGpsCoordinate>(BsonGpsCoordinate.CreateFromAppGpsCoordinate(contactInformation.GpsCoordinate)).AsBsonDocument :
                BsonNull.Value
        };
    }

    [BsonNoId]
    public class BsonWorkImage
    {
        [BsonElement("packageId")]
        public BsonString PackageID { get; set; } = BsonString.Empty;

        [BsonElement("url")]
        public BsonString Url { get; set; } = BsonString.Empty;

        [BsonIgnore]
        public WorkImage AsAppWorkImage => new WorkImage
        {
            PackageID = this.PackageID.AsString,
            Url = this.Url.AsString,
        };
    }

    private class BsonSkillProfile
    {
        [BsonId]
        public BsonObjectId Id { get; set; } = BsonObjectId.Empty;

        [BsonElement("name")]
        public BsonString Name { get; set; } = BsonString.Empty;

        [BsonElement("description")]
        public BsonString Description { get; set; } = BsonString.Empty;

        [BsonElement("industryCategoryId")]
        public BsonObjectId IndustryCategoryId { get; set; } = BsonObjectId.Empty;

        [BsonElement("industryId")]
        public BsonString IndustryId { get; set; } = BsonString.Empty;

        [BsonElement("userId")]
        public BsonObjectId UserId { get; set; } = BsonObjectId.Empty;

        [BsonElement("contactInformation")]
        public BsonDocument ContactInformation { get; set; } = new BsonDocument();

        [BsonElement("images")]
        public BsonArray Images { get; set; } = new BsonArray(); // of 'url' strings

        [BsonElement("workimages")]
        public BsonArray WorkImages { get; set; } = new BsonArray();

        [BsonElement("priority")]
        public BsonInt32 Priority { get; set; } = new BsonInt32(10);

        [BsonElement("packageStatus")]
        public BsonInt32 PackageStatus { get; set; } = new BsonInt32(0);
        
        [BsonElement("toplist")]
        public BsonBoolean TopList { get; set; } = BsonBoolean.False;

        [BsonIgnore]
        public SkillProfile AsAppSkillProfile => new SkillProfile
        {
            Id = this.Id.AsObjectId.ToString(),
            Name = this.Name.AsString,
            Description = this.Description.AsString,
            UserId = this.UserId.AsObjectId.ToString(),
            IndustryCategoryId = this.IndustryCategoryId.AsObjectId.ToString(),
            IndustryId = this.IndustryId.AsString,
            ContactInformation = BsonSerializer.Deserialize<BsonContactInformation>(this.ContactInformation.AsBsonDocument).AsAppContactInformation,
            Images = this.Images.AsEnumerable()
                        .Where(img => img.BsonType == BsonType.String)
                        .Select(bs => bs.AsString)
                        .ToList(),
            WorkImages = this.WorkImages.AsEnumerable()
                        .Where(img => img.BsonType == BsonType.Document)
                            .Select(bs => bs.AsBsonDocument)
                            .Select(bd => BsonSerializer.Deserialize<BsonWorkImage>(bd))
                            .Select(bi => bi.AsAppWorkImage)
                            .ToList(),
            Priority = this.Priority.AsInt32,
            PackageStatus = this.PackageStatus.AsInt32,
            TopList = this.TopList.AsBoolean,
        };

        public static BsonSkillProfile CreateFromAppSkillProfile(SkillProfile skillProfile, bool withNewId = true) => new BsonSkillProfile
        {
            Id = withNewId ? new BsonObjectId(new ObjectId()) : new BsonObjectId(new ObjectId(skillProfile.Id)),
            UserId = new BsonObjectId(new ObjectId(skillProfile.UserId)),
            Name = new BsonString(skillProfile.Name),
            Description = new BsonString(skillProfile.Description),
            IndustryCategoryId = new BsonObjectId(new ObjectId(skillProfile.IndustryCategoryId)),
            IndustryId = new BsonString(skillProfile.IndustryId),
            ContactInformation = BsonDocumentWrapper.Create<BsonContactInformation>(BsonContactInformation.CreateFromAppContactInformation(skillProfile.ContactInformation)),
            Images = new BsonArray(skillProfile.Images),
            WorkImages = BsonArray.Create(skillProfile.WorkImages.Select(m => new BsonDocument()
                                     {
                                         { "packageId", m.PackageID},
                                         { "url", m.Url }
                                     })),
            Priority = new BsonInt32(skillProfile.Priority),
            PackageStatus = new BsonInt32(skillProfile.PackageStatus),
            TopList = new BsonBoolean(skillProfile.TopList),
        };

        public static BsonSkillProfile FromCreateRequest(SkillProfileForCreate request, string ownerId, int priority) => new BsonSkillProfile
        {
            Id = new BsonObjectId(new ObjectId()),
            UserId = new BsonObjectId(new ObjectId(ownerId)),
            Name = new BsonString(request.Name),
            Description = new BsonString(request.Description),
            IndustryCategoryId = new BsonObjectId(new ObjectId(request.IndustryCategoryId)),
            IndustryId = new BsonString(request.IndustryId),
            ContactInformation = BsonDocumentWrapper.Create<BsonContactInformation>(BsonContactInformation.CreateFromAppContactInformation(request.ContactInformation)),
            Images = new BsonArray(request.Images),
            WorkImages = BsonArray.Create(request.WorkImages.Select(m => new BsonDocument()
                                     {
                                         { "packageId", m.PackageID},
                                         { "url", m.Url }
                                     })),
            Priority = new BsonInt32(priority),
            PackageStatus = new BsonInt32(request.PackageStatus),
            TopList = new BsonBoolean(request.TopList)
        };
    }

    private readonly MongoDatabaseContext _context;

    private readonly IMongoCollection<BsonSkillProfile> _collection;

    internal MongoSkillProfileRepository(MongoDatabaseContext context, string collectionName)
    {
        _context = context;
        _collection = _context.Database.GetCollection<BsonSkillProfile>(collectionName);
    }

    public async Task<IList<SkillProfile>> GetAllSkillProfiles()
    {
        var filter = Builders<BsonSkillProfile>.Filter.Empty;
        var result = await _collection.FindAsync(filter);
        return result.ToEnumerable().Select(item => item.AsAppSkillProfile).ToList();
    }

    public async Task<SkillProfile?> TryGetSkillProfileByIdAsync(string id)
    {
        var filter = Builders<BsonSkillProfile>.Filter.IdFilter(id);
        var result = await _collection.FindAsync(filter);
        var item = result.ToEnumerable().SingleOrDefault();
        return item?.AsAppSkillProfile;
    }

    public async Task<SkillProfile> InsertSkillProfile(SkillProfileForCreate request, string ownerId, int priority)
    {
        var document = BsonSkillProfile.FromCreateRequest(request, ownerId, priority);
        await _collection.InsertOneAsync(document);
        return document.AsAppSkillProfile;
    }

    public async Task<SkillProfile> UpdateSkillProfileAsync(string id, SkillProfile updated)
    {
        var filter = Builders<BsonSkillProfile>.Filter.IdFilter(id);
        var document = BsonSkillProfile.CreateFromAppSkillProfile(updated, false);
        var result = await _collection.FindOneAndReplaceAsync(filter, document, new FindOneAndReplaceOptions<BsonSkillProfile>{
            ReturnDocument = ReturnDocument.After
        });
        return result.AsAppSkillProfile;
    }

    public async Task<bool> RemoveSkillProfile(string id)
    {
        var filter = Builders<BsonSkillProfile>.Filter.IdFilter(id);
        var result = await _collection.DeleteOneAsync(filter);
        return result.DeletedCount == 1;
    }

    public async Task<SkillProfile> RemoveImageAsync(SkillProfile updated, string packageID)
    {
            var imgfilter = Builders<BsonSkillProfile>.Filter.And(
                        Builders<BsonSkillProfile>.Filter.Eq("workimages.packageId", packageID));

            var update = Builders<BsonSkillProfile>.Update.Pull("workimages", new BsonDocument(){
                                                                                    { "packageId", packageID }});

            var result = await _collection.FindOneAndUpdateAsync(imgfilter, update, new FindOneAndUpdateOptions<BsonSkillProfile>
            {
                ReturnDocument = ReturnDocument.After
            });
        
        return result.AsAppSkillProfile;
    }

    public async Task<IList<SkillProfile>> TryGetListSkillByUserIdAsync(string id)
    {
        var filter = Builders<BsonSkillProfile>.Filter.Empty;
        var result = await _collection.FindAsync(filter);
        var items = result.ToEnumerable().Select(item => item.AsAppSkillProfile).ToList();

        var newlist = items.Where(x => x.UserId == id).ToList();
        return newlist;
    }
}