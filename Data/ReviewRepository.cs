
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;

public interface IReviewRepository
{
    Task<IList<ServiceReview>> GetReviewsForService(string serviceId);
    Task<ServiceReview?> TryGetUserReviewForService(string userId, string serviceId);
    Task<ServiceReview> CreateReview(string userId, string serviceId, string comment, int rating);
    Task<ServiceReview> UpdateReview(string id, ServiceReview updated);
}

internal class MongoReviewRepository : IReviewRepository
{
    private class BsonServiceReview
    {
        [BsonId]
        public BsonObjectId Id { get; set; } = BsonObjectId.Empty;

        [BsonElement("serviceId")]
        public BsonObjectId ServiceId { get; set; } = BsonObjectId.Empty;

        [BsonElement("userId")]
        public BsonObjectId UserId { get; set; } = BsonObjectId.Empty;

        [BsonElement("comment")]
        public BsonString Comment { get; set; } = BsonString.Empty;

        [BsonElement("createdateTime")]
        public BsonString createdateTime { get; set; } = BsonString.Empty;

        [BsonElement("updatedateTime")]
        public BsonString updatedateTime { get; set; } = BsonString.Empty;

        [BsonElement("rating")]
        public BsonInt32 Rating { get; set; } = new BsonInt32(0);

        [BsonIgnore]
        public ServiceReview AsAppModel => new ServiceReview
        {
            Id = this.Id.AsObjectId.ToString(),
            ServiceId = this.ServiceId.AsObjectId.ToString(),
            UserId = this.UserId.AsObjectId.ToString(),
            Comment = this.Comment.AsString,
            Rating = this.Rating.AsInt32,
            CreateDateTime = this.createdateTime.AsString,
            UpdateDateTime = this.updatedateTime.AsString,
        };
    }

    private readonly MongoDatabaseContext _context;

    private readonly IMongoCollection<BsonServiceReview> _collection;

    internal MongoReviewRepository(MongoDatabaseContext context, string collectionName)
    {
        _context = context;
        _collection = _context.Database.GetCollection<BsonServiceReview>(collectionName);
    }

    public async Task<IList<ServiceReview>> GetReviewsForService(string serviceId)
    {
        var filter = Builders<BsonServiceReview>.Filter.Eq("serviceId", new ObjectId(serviceId));
        var result = await _collection.FindAsync(filter);
        return result.ToEnumerable().Select(bsonModel => bsonModel.AsAppModel).ToList();
    }

    public async Task<ServiceReview?> TryGetUserReviewForService(string userId, string serviceId)
    {
        var userFilter = Builders<BsonServiceReview>.Filter.Eq("userId", new ObjectId(userId));
        var serviceFilter = Builders<BsonServiceReview>.Filter.Eq("serviceId", new ObjectId(serviceId));
        var filter = Builders<BsonServiceReview>.Filter.And(new[] { userFilter, serviceFilter });
        var result = await _collection.FindAsync(filter);
        return result.ToEnumerable().SingleOrDefault()?.AsAppModel;
    }

    public async Task<ServiceReview> CreateReview(string userId, string serviceId, string comment, int rating)
    {
        var document = new BsonServiceReview
        {
            Id = new BsonObjectId(new ObjectId()),
            UserId = new BsonObjectId(new ObjectId(userId)),
            ServiceId = new BsonObjectId(new ObjectId(serviceId)),
            Comment = new BsonString(comment),
            Rating = new BsonInt32(rating),
            createdateTime = new BsonString(Convert.ToString(DateTime.UtcNow))
        };

        await _collection.InsertOneAsync(document);
        return document.AsAppModel;
    }

    public async Task<ServiceReview> UpdateReview(string id, ServiceReview updated)
    {
        var filter = Builders<BsonServiceReview>.Filter.IdFilter(id);
        var document = new BsonServiceReview
        {
            Id = new BsonObjectId(new ObjectId(updated.Id)),
            UserId = new BsonObjectId(new ObjectId(updated.UserId)),
            ServiceId = new BsonObjectId(new ObjectId(updated.ServiceId)),
            Comment = new BsonString(updated.Comment),
            Rating = new BsonInt32(updated.Rating),
            createdateTime = new BsonString(updated.CreateDateTime),
            updatedateTime = new BsonString(Convert.ToString(DateTime.UtcNow))
        };

        var result = await _collection.FindOneAndReplaceAsync(filter, document, new FindOneAndReplaceOptions<BsonServiceReview>
        {
            ReturnDocument = ReturnDocument.After
        });

        return result.AsAppModel;
    }
}
