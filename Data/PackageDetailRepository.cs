using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;

public interface IPackageDetailRepository
{
    Task<IList<Packages>> getPackages();
}

internal class MongoPackageDetailsRepository : IPackageDetailRepository
{
    private class BsonPackageDetails
    {
        [BsonId]
        public BsonObjectId Id { get; set; } = BsonObjectId.Empty;

        [BsonElement("packageName")]
        public BsonString packageName { get; set; } = BsonString.Empty;
        
        [BsonElement("packageType")]
        public BsonString packageType { get; set; } = BsonString.Empty;

        [BsonElement("price")]
        public BsonString price { get; set; } = BsonString.Empty;

        [BsonElement("expireDays")]
        public BsonString expireDays { get; set; } = BsonString.Empty;

        [BsonIgnore]
        public Packages AsPackages => new Packages
        {
            Id = this.Id.AsObjectId.ToString(),
            PackageName = this.packageName.AsString,
            PackageType = this.packageType.AsString,
            Price = this.price.AsString,
            ExpireDays = this.expireDays.AsString,
        };
    }

    private readonly MongoDatabaseContext _context;


    private readonly IMongoCollection<BsonPackageDetails> _collection;

    internal MongoPackageDetailsRepository(MongoDatabaseContext context, string collectionName)
    {
        _context = context;
        _collection = _context.Database.GetCollection<BsonPackageDetails>(collectionName);
    }

    public async Task<IList<Packages>> getPackages()
    {
        var filter = Builders<BsonPackageDetails>.Filter.Empty;
        var result = await _collection.FindAsync(filter);
        return result.ToEnumerable().Select(item => item.AsPackages).ToList();
    }
}