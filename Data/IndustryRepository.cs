using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;

public interface IIndustryRepository {
    Task<IList<IndustryCategory>> GetIndustryCategoriesAsync();

    Task<IList<IndustryCategoryAndIndustry>> GetIndustryName(string searchtext);
}

internal class MongoIndustryRepository: IIndustryRepository {
    [BsonNoId]
    private class BsonIndustry
    {
        [BsonElement("id")]
        public BsonString Id { get; set; } = BsonString.Empty;

        [BsonElement("name")]
        public BsonString Name { get; set; } = BsonString.Empty;

        [BsonIgnore]
        public Industry AsAppIndustry => new Industry
        {
            Id = this.Id.AsString,
            Name = this.Name.AsString
        };
    }

    private class BsonIndustryCategory
    {
        [BsonId]
        public BsonObjectId Id { get; set; } = BsonObjectId.Empty;

        [BsonElement("name")]
        public BsonString Name { get; set; } = BsonString.Empty;

        [BsonElement("image")]
        public BsonString Image { get; set; } = BsonString.Empty;

        [BsonElement("industries")]
        public BsonArray Industries { get; set; } = new BsonArray();

        [BsonIgnore]
        public IndustryCategory AsAppIndustryCategory => new IndustryCategory
        {
            Id = this.Id.AsObjectId.ToString(),
            Name = this.Name.AsString,
            Image = this.Image.AsString,
            Industries = this.Industries.AsEnumerable()
                            .Where(bv => bv.BsonType == BsonType.Document)
                            .Select(bs => bs.AsBsonDocument)
                            .Select(bd => BsonSerializer.Deserialize<BsonIndustry>(bd))
                            .Select(bi => bi.AsAppIndustry)
                            .ToList(),
        };
    }

    private readonly MongoDatabaseContext _context;

    private readonly IMongoCollection<BsonIndustryCategory> _collection;

    internal MongoIndustryRepository(MongoDatabaseContext context, string collectionName)
    {
        _context = context;
        _collection = _context.Database.GetCollection<BsonIndustryCategory>(collectionName);
    }

    public async Task<IList<IndustryCategory>> GetIndustryCategoriesAsync()
    {
        var filter = Builders<BsonIndustryCategory>.Filter.Empty;
        var result = await _collection.FindAsync(filter);
        return result.ToEnumerable().Select(item => item.AsAppIndustryCategory).ToList();
    }

    public async Task<IList<IndustryCategoryAndIndustry>> GetIndustryName(string searchtext)
    {
        var filter = Builders<BsonIndustryCategory>.Filter.Empty;
        var result = await _collection.FindAsync(filter);
        var lstCategory = result.ToEnumerable().Select(item => item.AsAppIndustryCategory).ToList();
        var objResult = (from cat in lstCategory
                         from i in cat.Industries
                         where i.Name.ToLower() == searchtext.ToLower()
                            select new IndustryCategoryAndIndustry
                            {
                                Id = cat.Id,
                                Name = cat.Name,
                                Image = cat.Image,
                                IndustryId = i.Id,
                                IndustryName = i.Name,
                            }).Take(1).ToList();

        return objResult; 
    }
}

internal class MockIndustryRepository: IIndustryRepository {
    public async Task<IList<IndustryCategory>> GetIndustryCategoriesAsync()
    {
        return new List<IndustryCategory> {
            new IndustryCategory { Id = "8183d3ec-90e7-4fb9-996d-e3e14fb967be", Name = "Person Care", Industries = new List<Industry>{
                new Industry { Id = "b71afde4-1fda-49e9-a74a-5b923b0936b1", Name = "Barber" },
                new Industry { Id = "916cb138-011b-4e34-8a69-1ad0fefa420f", Name = "Cosmetic Products" },
                new Industry { Id = "fea314ce-9897-4503-ad6b-1bb2c817f213", Name = "Women's Hair" },
                new Industry { Id = "7dead1fd-7c1a-411e-b203-61aa6a8bc09e", Name = "Women's Nails" },
                new Industry { Id = "da8a22e1-44ec-4ee0-9933-2577993e0140", Name = "Women's Make-up" },
                new Industry { Id = "afab98f4-3c48-4ece-8384-7ae78abdbbc7", Name = "Women's Lashes" },
                new Industry { Id = "79345770-53a0-4e63-b833-ac5614227f8c", Name = "Personal Trainer" },
                new Industry { Id = "0606deae-d2c5-4db6-88f6-0235e1d7c18a", Name = "Yoga Instructor" },
                new Industry { Id = "e3b2ad9e-6609-4229-8c18-2d7fd83a6ebf", Name = "Massage Therapist" },
                new Industry { Id = "48546af1-8fd0-4139-96a6-1f993d745644", Name = "Life Coach" },
                new Industry { Id = "c6878f5d-3796-46e4-9419-af198b785d23", Name = "Mentorship" },
            }},
            new IndustryCategory { Id = "6b8f7bf3-49ef-425b-8b1b-c24c9d9312cc", Name = "Auto", Industries = new List<Industry>{
                new Industry { Id = "8d2e9d3f-12f1-4a89-ae83-d32c035d160c", Name = "Mechanic" },
                new Industry { Id = "b957bb77-f7e0-408f-89af-04f0bd084ddb", Name = "Detailing" },
                new Industry { Id = "ea15102a-d22e-42b8-8d1a-6ce1b8ee0137", Name = "Paint" },
                new Industry { Id = "8391e85a-343f-4424-addd-c593a5137731", Name = "Tires" },
                new Industry { Id = "05d6dd7b-bf44-438e-9940-db32df689822", Name = "Parts" },
                new Industry { Id = "e581773c-6c1f-41a7-82df-88f296d0fad6", Name = "All Repair" },
                new Industry { Id = "d60f394a-a784-49b4-8476-77cf9b2b67be", Name = "Auto Sale" }
            }},
            new IndustryCategory { Id = "1b386bc6-767a-49b8-9d9d-606f74d37729", Name = "Clothing/Shoes", Industries = new List<Industry>{
                new Industry { Id = "e88e5650-d196-47c0-bf65-85e63c23750d", Name = "Men" },
                new Industry { Id = "44c894a3-2dce-4edf-a986-e821bed35185", Name = "Women" },
                new Industry { Id = "04fbcc17-5239-4717-88c3-1b6b99cf6175", Name = "Kids" },
                new Industry { Id = "0c0d8a3e-eab4-4ebc-9153-29d1c51ff13d", Name = "Stylist" }
            }},
            new IndustryCategory { Id = "466de2b9-169c-4b76-a6a5-cc7854313ff2", Name = "Food", Industries = new List<Industry>{
                new Industry { Id = "2f38a765-c5e0-49d1-8a99-1b7a6f3fcf9e", Name = "Catering" },
                new Industry { Id = "aa74078f-87af-4bba-8b61-850b2df46384", Name = "Personal Chef" },
                new Industry { Id = "bc3ae63d-dc9d-4b58-b16a-e7564f0c0297", Name = "Recipe Tutorial" }
            }},
            new IndustryCategory { Id = "118b9b84-e9b1-43e8-a769-ed35a8176915", Name = "Sports", Industries = new List<Industry>{
                new Industry { Id = "9c554c62-cc1c-43cf-9981-341a2ae12406", Name = "Skills Coach" }
            }},
            new IndustryCategory { Id = "78396214-b1cb-419d-80ef-a21af2f600b9", Name = "Pets", Industries = new List<Industry>{
                new Industry { Id = "5e64b8a3-dec2-4973-84a8-4cc822272d57", Name = "Pet Sitter" },
                new Industry { Id = "d7103450-468f-4752-aaa8-a4e83fd233cf", Name = "Dog Walker" },
                new Industry { Id = "f3318a70-022e-4560-aa9e-b026a92b618d", Name = "Pet Care Products" },
                new Industry { Id = "6a3aca7c-936e-4154-8381-a30fcf243d8e", Name = "Toys" }
            }},
            new IndustryCategory { Id = "c2219e47-30d6-47a7-bbcc-b7ffe527b906", Name = "Service To Person", Industries = new List<Industry>{
                new Industry { Id = "9325cf15-c02b-469e-8a21-0a1d62fb7454", Name = "Homework Aid" },
                new Industry { Id = "8081217c-b16a-478f-aa19-0e69b32fb1b0", Name = "Personal Teacher" },
                new Industry { Id = "d76ef326-8379-47a8-9fdc-c61cb942d590", Name = "Baby Sitter" },
                new Industry { Id = "567d56d6-b8bd-401a-8103-fd065aa20830", Name = "Artistic Lessons" },
                new Industry { Id = "8f60d429-8984-42b0-91d3-6caed4d8a6ff", Name = "Transportation" }
            }},
            new IndustryCategory { Id = "8adf3c84-a346-4360-85c6-c42f449ebffd", Name = "Home Care", Industries = new List<Industry>{
                new Industry { Id = "7e98ee63-d75a-4bf4-8825-e6a9c7b07ab3", Name = "Cleaning" },
                new Industry { Id = "7d988fe7-9210-4a1f-8a89-487bdd8a40ec", Name = "Landscaping" },
                new Industry { Id = "ab88093f-45d1-480d-87bc-b2cef2e0ac33", Name = "TV Mounting" },
                new Industry { Id = "4f2972ed-8c4b-470b-b4a2-e725a48e1af3", Name = "Plumbing" },
                new Industry { Id = "29184221-366c-4b58-a07e-3fb1cef61f86", Name = "Roofing" },
                new Industry { Id = "021f9cf9-f330-4414-849d-3e59e426352d", Name = "Electrician" },
                new Industry { Id = "60fd6323-0d1b-4706-b5e6-6d10b0030aaf", Name = "Furniture Assembly" },
                new Industry { Id = "b59009d5-f15c-4ec5-9e2a-3a37517b75b8", Name = "Furniture Sales" },
                new Industry { Id = "b0d69b39-0554-435a-9cbf-7d27f8606330", Name = "Paint" }
            }},
            new IndustryCategory { Id = "5f3e96c3-621f-4c38-a2ff-782414e90114", Name = "Technology", Industries = new List<Industry>{
                new Industry { Id = "73acd6a4-1f27-4085-bf5c-d0a9510b9f62", Name = "Website Design" },
                new Industry { Id = "34fc525d-40f3-4c57-b7ff-2110c42fd484", Name = "App Developer" },
                new Industry { Id = "a26c9bb7-8338-467b-b5f2-a56847695ec0", Name = "Teacher" }    
            }},
            new IndustryCategory { Id = "6dae12e6-5738-4ddc-93be-cfb7b3d6fe09", Name = "Other", Industries = new List<Industry>{
                new Industry { Id = "95b74b6a-8a06-4d76-8aa7-48478d9947bc", Name = "Other" }
            }}
        };
    }

    public async Task<IList<IndustryCategoryAndIndustry>> GetIndustryName(string searchtext)
    {
        return new List<IndustryCategoryAndIndustry>{
                new IndustryCategoryAndIndustry { Id = "8adf3c84-a346-4360-85c6-c42f449ebffd", Name = "Home Care", Image="" ,IndustryId = "b71afde4-1fda-49e9-a74a-5b923b0936b1", IndustryName = "Barber" },
              
            };
    }
}
