using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;

public interface IPackageRepository
{
    Task<PackageInfo> InsertPackageInfo(PackageDetails request);
    Task<PackageInfo?> TryGetPackageIdAsync(string id);
    Task<IList<PackageInfo>> GetPackageList();

    Task<PackageInfo> UpdatePackageInfoAsync(PackageInfo updated);
}

internal class MongoPackageRepository : IPackageRepository
{
    private class BsonPackageInfo
    {
        [BsonId]
        public BsonObjectId Id { get; set; } = BsonObjectId.Empty;

        [BsonElement("userID")]
        public BsonString userID { get; set; } = BsonString.Empty;
        
        [BsonElement("serviceID")]
        public BsonString serviceID { get; set; } = BsonString.Empty;

        [BsonElement("masterPackageID")]
        public BsonString masterPackageID { get; set; } = BsonString.Empty;

        [BsonElement("packageName")]
        public BsonString packageName { get; set; } = BsonString.Empty;

        [BsonElement("imageCount")]
        public BsonInt32 imageCount { get; set; } = new BsonInt32(0);

        [BsonElement("expireTime")]
        public BsonString expireTime { get; set; } = BsonString.Empty;

        [BsonElement("packageStatus")]
        public BsonInt32 packageStatus { get; set; } = new BsonInt32(0);
        
        [BsonElement("createdateTime")]
        public BsonString createdateTime { get; set; } = BsonString.Empty;

        [BsonElement("totalImageCount")]
        public BsonInt32 totalImageCount { get; set; } = new BsonInt32(0);

        [BsonElement("serviceName")]
        public BsonString ServiceName { get; set; } = BsonString.Empty;

        public static BsonPackageInfo FromCreateInfo(PackageDetails request) => new BsonPackageInfo
        {
            Id = new BsonObjectId(new ObjectId()),
            userID = new BsonString(request.UserID),
            serviceID = new BsonString(request.ServiceID),
            masterPackageID = new BsonString(request.MasterPackageID),
            packageName = new BsonString(request.PackageName),
            imageCount = new BsonInt32(request.ImageCount),
            expireTime = new BsonString(request.ExpireTime),
            packageStatus = new BsonInt32(request.PackageStatus),
            createdateTime = new BsonString(Convert.ToString(DateTime.UtcNow)),
            totalImageCount = new BsonInt32(request.TotalImageCount),
            ServiceName = new BsonString(request.ServiceName)
        };

        [BsonIgnore]
        public PackageInfo AsPackageInfo => new PackageInfo
        {
            Id = this.Id.AsObjectId.ToString(),
            PackageName = this.packageName.AsString,
            ServiceID = this.serviceID.AsString,
            MasterPackageID = this.masterPackageID.AsString,
            UserID = this.userID.AsString,
            ImageCount = this.imageCount.AsInt32,
            ExpireTime = this.expireTime.AsString,
            PackageStatus = this.packageStatus.AsInt32,
            CreateDateTime = this.createdateTime.AsString,
            TotalImageCount = this.totalImageCount.AsInt32,
            ServiceName = this.ServiceName.AsString
        };

        public static BsonPackageInfo CreateFromPackageInfo(PackageInfo packageInfo) => new BsonPackageInfo
        {
            Id = new BsonObjectId(new ObjectId(packageInfo.Id)),
            userID = new BsonString(packageInfo.UserID),
            serviceID = new BsonString(packageInfo.ServiceID),
            masterPackageID = new BsonString(packageInfo.MasterPackageID),
            packageName = new BsonString(packageInfo.PackageName),
            imageCount = new BsonInt32(packageInfo.ImageCount),
            expireTime = new BsonString(packageInfo.ExpireTime),
            packageStatus = new BsonInt32(packageInfo.PackageStatus),
            createdateTime = new BsonString(Convert.ToString(DateTime.UtcNow)),
            totalImageCount = new BsonInt32(packageInfo.TotalImageCount),
            ServiceName = new BsonString(packageInfo.ServiceName)
        };
    }

    private readonly MongoDatabaseContext _context;


    private readonly IMongoCollection<BsonPackageInfo> _collection;

    internal MongoPackageRepository(MongoDatabaseContext context, string collectionName)
    {
        _context = context;
        _collection = _context.Database.GetCollection<BsonPackageInfo>(collectionName);
    }

    public async Task<PackageInfo> InsertPackageInfo(PackageDetails request)
    {
        var document = BsonPackageInfo.FromCreateInfo(request);
        await _collection.InsertOneAsync(document);
        return document.AsPackageInfo;
    }

    public async Task<PackageInfo?> TryGetPackageIdAsync(string id)
    {
        var filter = Builders<BsonPackageInfo>.Filter.IdFilter(id);
        var result = await _collection.FindAsync(filter);
        var item = result.ToEnumerable().SingleOrDefault();
        return item?.AsPackageInfo;
    }

    public async Task<IList<PackageInfo>> GetPackageList()
    {
        var filter = Builders<BsonPackageInfo>.Filter.Empty;
        var result = await _collection.FindAsync(filter);
        return result.ToEnumerable().Select(item => item.AsPackageInfo).ToList();
    }

    public async Task<PackageInfo> UpdatePackageInfoAsync(PackageInfo updated)
    {
        var filter = Builders<BsonPackageInfo>.Filter.IdFilter(updated.Id);
        var document = BsonPackageInfo.CreateFromPackageInfo(updated);
        var result = await _collection.FindOneAndReplaceAsync(filter, document, new FindOneAndReplaceOptions<BsonPackageInfo>
        {
            ReturnDocument = ReturnDocument.After
        });
        return result.AsPackageInfo;
    }
}

