using System;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;

public interface IUserRepository {
    Task<UserProfile?> TryGetUserProfileFromClaimAsync(string adUserIdClaim);
    
    Task<UserProfile?> TryGetUserProfileAsync(string id);

    Task<UserProfile> CreateNewUserProfileAsync(string adUserIdClaim, string displayNameClaim);

    Task<UserProfile> UpdateUserProfileAsync(string id, UserProfile updated);
}

internal class MongoUserRepository : IUserRepository {
    [BsonNoId]
    private class BsonMembershipInformation
    {
        [BsonElement("membershipLevel")]
        public BsonString MembershipLevel { get; set; } = BsonString.Empty;

        [BsonElement("membershipProvider")]
        public BsonString MembershipProvider { get; set; } = BsonString.Empty;

        [BsonIgnore]
        public MembershipInformation AsAppMembershipInformation => new MembershipInformation {
            Level = Enum.Parse<MembershipInformation.MembershipLevel>(this.MembershipLevel.AsString, true),
            Provider = Enum.Parse<MembershipInformation.MembershipProvider>(this.MembershipProvider.AsString, true),
        };

        public static BsonMembershipInformation FromApp(MembershipInformation info) => new BsonMembershipInformation {
            MembershipLevel = new BsonString(info.Level.ToString()),
            MembershipProvider = new BsonString(info.Provider.ToString())
        };

        public static BsonMembershipInformation Default => new BsonMembershipInformation {
            MembershipLevel = new BsonString(MembershipInformation.MembershipLevel.NONE.ToString()),
            MembershipProvider = new BsonString(MembershipInformation.MembershipProvider.UNKNOWN.ToString())
        };
   }

    private class BsonUserProfile
    {
        [BsonId]
        public BsonObjectId Id { get; set; } = BsonObjectId.Empty;

        [BsonElement("adUserId")]
        public BsonString ADUserId { get; set; } = BsonString.Empty;

        [BsonElement("displayName")]
        public BsonString DisplayName { get; set; } = BsonString.Empty;

        [BsonElement("membershipInformation")]
        public BsonDocument MembershipInformation { get; set; } = new BsonDocument();

        [BsonIgnore]
        public UserProfile AsAppUserProfile => new UserProfile
        {
            Id = this.Id.AsObjectId.ToString(),
            ADUserId = this.ADUserId.AsString,
            DisplayName = this.DisplayName.AsString,
            Membership = BsonSerializer.Deserialize<BsonMembershipInformation>(this.MembershipInformation.AsBsonDocument).AsAppMembershipInformation
        };

        public static BsonUserProfile FromApp(UserProfile appProfile) => new BsonUserProfile
        {
            Id = new BsonObjectId(new ObjectId(appProfile.Id)),
            ADUserId = new BsonString(appProfile.ADUserId),
            DisplayName = new BsonString(appProfile.DisplayName),
            MembershipInformation = BsonDocumentWrapper.Create<BsonMembershipInformation>(BsonMembershipInformation.FromApp(appProfile.Membership))
        };

        public static BsonUserProfile FromClaims(string adUserIdClaim, string displayNameClaim) => new BsonUserProfile
        {
            Id = new BsonObjectId(new ObjectId()),
            ADUserId = new BsonString(adUserIdClaim),
            DisplayName = new BsonString(displayNameClaim),
            MembershipInformation = BsonDocumentWrapper.Create<BsonMembershipInformation>(BsonMembershipInformation.Default).AsBsonDocument
        };
    }

    private readonly MongoDatabaseContext _context;
    
    private readonly IMongoCollection<BsonUserProfile> _collection;

    internal MongoUserRepository(MongoDatabaseContext context, string collectionName)
    {
        _context = context;
        _collection = _context.Database.GetCollection<BsonUserProfile>(collectionName);
    }

    public async Task<UserProfile?> TryGetUserProfileFromClaimAsync(string adUserIdClaim)
    {
        var filter = Builders<BsonUserProfile>.Filter.Eq("adUserId", adUserIdClaim);
        var result = await _collection.FindAsync(filter);
        var item = result.ToEnumerable().SingleOrDefault();
        return item?.AsAppUserProfile;
    }

    public async Task<UserProfile?> TryGetUserProfileAsync(string id)
    {
        var filter = Builders<BsonUserProfile>.Filter.IdFilter(id);
        var result = await _collection.FindAsync(filter);
        var item = result.ToEnumerable().SingleOrDefault();
        return item?.AsAppUserProfile;
    }

    public async Task<UserProfile> CreateNewUserProfileAsync(string adUserIdClaim, string displayNameClaim)
    {
        var document = BsonUserProfile.FromClaims(adUserIdClaim, displayNameClaim);
        await _collection.InsertOneAsync(document);
        return document.AsAppUserProfile;
    }

    public async Task<UserProfile> UpdateUserProfileAsync(string id, UserProfile updated)
    {
        var filter = Builders<BsonUserProfile>.Filter.IdFilter(id);
        var document = BsonUserProfile.FromApp(updated);
        var result = await _collection.FindOneAndReplaceAsync(filter, document, new FindOneAndReplaceOptions<BsonUserProfile>{
            ReturnDocument = ReturnDocument.After
        });
        return result.AsAppUserProfile;
    }
}