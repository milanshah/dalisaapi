using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using MongoDB.Driver.GeoJsonObjectModel;

public interface ISearchRecordRepository
{
    Task<IEnumerable<SearchRecord>> GetRecentSearchesNearby(double longitude, double latitude, TimeSpan interval);
    Task<SearchRecord> RecordSearchNear(double longitude, double latitude, string text);
}

internal class MongoSearchRecordRepository : ISearchRecordRepository
{
    private class BsonSearchRecord
    {
        [BsonId]
        public BsonObjectId Id { get; set; } = new BsonObjectId(new ObjectId());

        [BsonElement("created")]
        public BsonDateTime Created { get; set; } = new BsonDateTime(DateTime.UtcNow);

        [BsonElement("location")]
        public GeoJsonPoint<GeoJson2DCoordinates> Location { get; set; } = new GeoJsonPoint<GeoJson2DCoordinates>(new GeoJson2DCoordinates(0.0, 0.0));

        [BsonElement("text")]
        public BsonString Text { get; set; } = BsonString.Empty;

        [BsonIgnore]
        public SearchRecord AsAppModel => new SearchRecord
        {
            Longitude = Location.Coordinates.X,
            Latitude = Location.Coordinates.Y,
            Text = Text.AsString
        };
    }

    private readonly MongoDatabaseContext _context;
    private readonly IMongoCollection<BsonSearchRecord> _collection;

    internal MongoSearchRecordRepository(MongoDatabaseContext context, string collectionName)
    {
        _context = context;
        _collection = _context.Database.GetCollection<BsonSearchRecord>(collectionName);
    }

    public async Task<IEnumerable<SearchRecord>> GetRecentSearchesNearby(double longitude, double latitude, TimeSpan interval)
    {
        var geoPoint = new GeoJsonPoint<GeoJson2DCoordinates>(new GeoJson2DCoordinates(longitude, latitude));
        var lastWeek = new BsonDateTime(DateTime.UtcNow.Subtract(interval));
        
        var locationFilter = Builders<BsonSearchRecord>.Filter.NearSphere<GeoJson2DCoordinates>(record => record.Location, geoPoint, 5 * 1609.344);
        var timeFilter = Builders<BsonSearchRecord>.Filter.Gte<BsonDateTime>(record => record.Created, lastWeek);
        var filter = Builders<BsonSearchRecord>.Filter.And(new[] { locationFilter, timeFilter });
        
        var result = await _collection.FindAsync(filter);
        return result.ToEnumerable().Select(item => item.AsAppModel);
    }

    public async Task<SearchRecord> RecordSearchNear(double longitude, double latitude, string text)
    {
        var document = new BsonSearchRecord
        {
            Id = new BsonObjectId(new ObjectId()),
            Created = new BsonDateTime(DateTime.UtcNow),
            Location = new GeoJsonPoint<GeoJson2DCoordinates>(new GeoJson2DCoordinates(longitude, latitude)),
            Text = new BsonString(text)
        };

        await _collection.InsertOneAsync(document);
        return document.AsAppModel;
    }
}