using System.Security.Authentication;
using MongoDB.Bson;
using MongoDB.Driver;

public static class MongoExtensions
{
    public static FilterDefinition<T> IdFilter<T>(this FilterDefinitionBuilder<T> builder, string id)
    {
        return builder.Eq("_id", new ObjectId(id));
    }
}

public class MongoDatabaseContext
{
    private readonly string ConnectionString;
    
    public readonly IMongoDatabase Database;

    public MongoDatabaseContext(string connectionString, string databaseName)
    {
        ConnectionString = connectionString;

        var settings = MongoClientSettings.FromUrl(new MongoUrl(ConnectionString));
        settings.SslSettings = new SslSettings { EnabledSslProtocols = SslProtocols.Tls12 };
        var client = new MongoClient(settings);

        Database = client.GetDatabase(databaseName);
    }
}